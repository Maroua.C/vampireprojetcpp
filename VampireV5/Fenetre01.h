#ifndef FENETRE01_H_INCLUDED
#define FENETRE01_H_INCLUDED

// on appele pas tout la librairie mais uniquement les parties qui nous interesse cela évite de charger la librairie en entier
#include <gtkmm/window.h>
#include <gtkmm/button.h>
#include <gtkmm/stock.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/image.h>
#include <gtkmm/entry.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/messagedialog.h>

#include <gtkmm/scrolledwindow.h>


//utilisation de string d'ou l'appelation de la librairie adéquate
#include <string>

using namespace std;



/***********************************************/
//Classe pour afficher la partie graphique hérite de gtk::Window
class Fenetre : public Gtk::Window
{
    public :
        //constructeur
        Fenetre() ;

        typedef sigc::signal<void> type_signal_taille_max ;
        type_signal_taille_max  signal_taille_max() ;

        void augmenterTaille();
        void augmenterTaille2(int augmentationLargeur, int augmentationHauteur);

        //permet de lire lefichier de resultat et de trouver si le nom a deja ete saisie d'ou le bool
        bool lecturefichierV (Glib::ustring nomSaisiGet);

        //permet de valider la saisi de l'utilisateur et de faire des controles necessaire sur certains parametre par exemple le nom ou les attributs...
        void validationDeLaSaisie() ;

    protected :
        type_signal_taille_max signalTailleMax ;

    private :
        // Bare de defilement
        Gtk::ScrolledWindow barresDeDefefilement;


        //création d'un bouton permettant de valider et un autre bouton pour quitter
        Gtk::Button boutonValider;
        Gtk::Button boutonQuitter;

/* ********************************** */
/* ********************************** */
       //partie des widgets

    //Declaration de boites donc des widgets de type HBox ou VBox en fonction des besoins
        // Creation d'une boite verticale (vbox) avec 2 parametres
       Gtk::VBox boite1;

        // creation des boites horizontales (Hbox)
       Gtk::HBox boite2; // Contient les zones a saisir (name, nature, clan  )
       Gtk::HBox boite3; // Contient les zones a saisir (Player, Demener, chronide)

       Gtk::HBox boite_points ; // Contient le text "Attributes: 7/5/3 • Abilities:13/9/5 • Disciplines:3 • Backgrounds:5 • Virtues:7"

       Gtk::HBox boite_attribut_ ; // Contient le text "--------------- Attribut-------------------"
       Gtk::HBox boite4; // Contient les zones a saisir (Generation, concept, Sire)
       Gtk::HBox boite5 ; // Contient les attributs
       Gtk::VBox boite51 ; // contient les attributs Physique
       Gtk::HBox boite511 ; // contient l'attribut Physique Strength
       Gtk::HBox boite512 ; // contient l'attribut Physique Dexterity
       Gtk::HBox boite513 ; // contient l'attribut Physique Stamina
       Gtk::VBox boite52 ; // contient l'attribut Social
       Gtk::HBox boite521 ; // contient l'attribut Social Charisma
       Gtk::HBox boite522 ; // contient l'attribut Social Manipulation
       Gtk::HBox boite523 ; // contient l'attribut Social Appearance
       Gtk::VBox boite53 ; // contient l'attribut Mental
       Gtk::HBox boite531 ; // contient l'attribut Mental Perception
       Gtk::HBox boite532 ; // contient l'attribut Mental Intellegence
       Gtk::HBox boite533 ; // contient l'attribut Mental Wits

/* ********************************** */

       Gtk::HBox boite_abilities_ ; // Contient le text "--------------- abilities-------------------"

       Gtk::HBox boite6 ; // Contient les abilities
       //Talent
       Gtk::VBox boite61 ; // contient les abilities Talent
       Gtk::HBox boite611 ; // contient les   Talent Alertness
       Gtk::HBox boite612 ; // contient les   Talent Athletics
       Gtk::HBox boite613 ; // contient les   Talent Awareness
       Gtk::HBox boite614 ; // contient les   Talent brawl
       Gtk::HBox boite615 ; // contient les   Talent
       Gtk::HBox boite616 ; // contient les   Talent
       Gtk::HBox boite617 ; // contient les   Talent
       Gtk::HBox boite618 ; // contient les   Talent
       Gtk::HBox boite619 ; // contient les   Talent
       Gtk::HBox boite610 ; // contient les   Talent
       Gtk::HBox boite6101 ; // contient les   Talent

       //skills
       Gtk::VBox boite62 ; // contient les abilities skills
       Gtk::HBox boite621 ; // contient les   skills Animal Ken
       Gtk::HBox boite622 ; // contient les   skills Crafts
       Gtk::HBox boite623 ; // contient les   skills Drive
       Gtk::HBox boite624 ; // contient les   skills
       Gtk::HBox boite625 ; // contient les   skills
       Gtk::HBox boite626 ; // contient les   skills
       Gtk::HBox boite627 ; // contient les   skills
       Gtk::HBox boite628 ; // contient les   skills
       Gtk::HBox boite629 ; // contient les   skills
       Gtk::HBox boite620 ; // contient les   skills
       Gtk::HBox boite6201 ; // contient les   skills


        //Knowledges
       Gtk::VBox boite63 ; // contient les abilities Knowledges
       Gtk::HBox boite631 ; // contient les   Knowledges Academics
       Gtk::HBox boite632 ; // contient les   Knowledges Computer
       Gtk::HBox boite633 ; // contient les   Knowledges Finance
       Gtk::HBox boite634 ; // contient les   Knowledges
       Gtk::HBox boite635 ; // contient les   Knowledges
       Gtk::HBox boite636 ; // contient les   Knowledges
       Gtk::HBox boite637 ; // contient les   Knowledges
       Gtk::HBox boite638 ; // contient les   Knowledges
       Gtk::HBox boite639 ; // contient les   Knowledges
       Gtk::HBox boite630 ; // contient les   Knowledges
       Gtk::HBox boite6301 ; // contient les   Knowledges


/* ********************************** */

       Gtk::HBox boite_advantages_ ; // Contient le text "--------------- Advantages-------------------"
       Gtk::HBox boite7 ; // Contient les Avantages
       Gtk::VBox boite71 ;  // Disciplines
       Gtk::HBox boite711 ;
       Gtk::HBox boite712 ;
       Gtk::HBox boite713 ;
       Gtk::HBox boite714 ;
       Gtk::HBox boite715 ;
       Gtk::HBox boite716 ;
       Gtk::VBox boite72 ; // contient l'attribut Background
       Gtk::HBox boite721 ;
       Gtk::HBox boite722 ;
       Gtk::HBox boite723 ;
       Gtk::HBox boite724 ;
       Gtk::HBox boite725 ;
       Gtk::HBox boite726 ;
       Gtk::VBox boite73 ; // contient l'attribut Virtue
       Gtk::HBox boite731 ;
       Gtk::HBox boite732 ;
       Gtk::HBox boite733 ;


/* ********************************** */
        //se situe juste apres Advantage
        Gtk::HBox boite_transition_;

       Gtk::HBox boite8 ; // Contient les Humanity/Path, Health,Blood, will power ...
       //Type Here
       Gtk::VBox boite81 ; // contient le [Type Here]
       Gtk::HBox boite811 ; // contient la zone de saisie 1
       Gtk::HBox boite812 ; // contient la zone de saisie 2

        //Humanity/Path Willpower et tout
       Gtk::VBox boite82 ; // contient le Humanity/Path Willpower et tout
       Gtk::HBox boite821 ; // contient le Humanity/Path Name
       Gtk::HBox boite822 ; // contient le Humanity/Path saisie


       Gtk::HBox boite823 ; // contient Willpower Name
       Gtk::HBox boite824 ; // contient Willpower saisie

       Gtk::HBox boite825 ; // contient la Blood Pool Name
       Gtk::HBox boite826 ; // contient Blood Pool saisie


        //Health  Weakness Experience
       Gtk::VBox boite83 ; // contient le Health
       Gtk::HBox boite831 ; // contient le Health Bruised
       Gtk::HBox boite832 ; // contient le Health Hurt
       Gtk::HBox boite833 ; // contient le Health Injured
       Gtk::HBox boite834 ; // contient le Health Wounded
       Gtk::HBox boite835 ; // contient le Health Mauled
       Gtk::HBox boite836 ; // contient le Health Crippled
       Gtk::HBox boite837 ; // contient le Health Incapacitated


       Gtk::HBox boite838 ; // contient le Weakness
       Gtk::HBox boite839 ; // contient le Weakness Entry

       Gtk::HBox boite830 ; // contient le Experience
       Gtk::HBox boite8301 ; // contient le Experience Entry

/* ********************************** */
       Gtk::HBox boite_meritsANDflaws_ ; // Contient le text "--------------- Merits and Flaws-------------------"
       Gtk::HBox boite9 ; // Contient les merit  and flows
       Gtk::VBox boite91 ;
       Gtk::VBox boite92 ;
       Gtk::VBox boite93 ;
       Gtk::VBox boite94 ;
       Gtk::VBox boite95 ;
       Gtk::VBox boite96 ;
       Gtk::VBox boite97 ;

/* ********************************** */
    //PARTIE Other Traits
       Gtk::HBox boite_otherTraits_ ; // Contient le text "-----------------------------------"
       Gtk::HBox boite1011 ;  // contient le otherTraits1Entry et listeDeroulanteOtherTraits1
       Gtk::HBox boite1022 ;  // contient le otherTraits2Entry  et listeDeroulanteOtherTraits2
       Gtk::HBox boite1033 ;  // contient le otherTraits3Entry et listeDeroulanteOtherTraits3

       Gtk::HBox boite101 ;  
       Gtk::HBox boite102 ;  
       Gtk::HBox boite103 ;  

       Gtk::VBox boite10 ;


/* ********************************** */
    //Rituals  and Paths
        Gtk::HBox boite_ritualsPaths_ ; // Contient le text "---------------Rituals  and Paths-------------------"

       Gtk::VBox boite21 ; 

       Gtk::VBox boite211 ;  
       Gtk::VBox boite212 ;  

       Gtk::HBox boite2122 ;  // contient le listeDeroulantepathsName et listeDeroulantepathsLevel
       Gtk::HBox boite2121 ;  // contient le pathsNameLabel 

       Gtk::HBox boite2112 ;  // contient le listeDeroulanteritualsName et listeDeroulanteritualsLevel
       Gtk::HBox boite2111 ;  // contient le ritualsNameLabel et ritualsLevelLabel

/* ********************************** */
    // Experience  Derangements
       Gtk::HBox boite_experienceDerangements_ ; // Contient le text "--------------- Experience  Derangements ------------------"

        Gtk::HBox boite12 ;

       Gtk::VBox boite121 ;
       Gtk::VBox boite1200 ; // contient le experienceDLabel 
       Gtk::HBox boite1201 ; // contient le totalEntry et totalLabel
       Gtk::HBox boite1202 ; // contient le totalSpentEntry et totalSpentLabel
       Gtk::HBox boite1203 ; // contient le spentOnLabel
       Gtk::HBox boite1204 ; // contient le spentOnEntry

       Gtk::VBox boite122 ;
       Gtk::VBox boite1220; // contient le derangementsLabel
       Gtk::HBox boite1221; // contient le derangementsEntry

/* ********************************** */
        //Combat
        Gtk::HBox boite_combat_ ; // Contient le text "--------------- Combat-------------------"

       Gtk::HBox boite13 ; 

       Gtk::VBox boite131 ;
       Gtk::HBox boite1311 ;    // contient le combatWeaponAttackLabel et combatDiffLabel et combatRangeLabel,combatRateLabel,combatClipLabel,combatConcealLabel
       Gtk::HBox boite1312 ; // contient le combatWeaponAttackEntry et combatDiffEntry et combatRangeEntry, combatRateEntry,combatClipEntry,combatConcealEntry

        //meme principe que la boite131 et boite 1311, boite1312 dans la repartision des Entry et Label
       Gtk::VBox boite132 ; 
       Gtk::HBox boite1321 ; 
       Gtk::HBox boite1322 ; 
       Gtk::HBox boite1323 ; 
       Gtk::HBox boite1324 ; 
       Gtk::HBox boite1325 ; 
       Gtk::HBox boite1326 ;

/* ********************************** */
        //Expanded Backgrounds
         Gtk::HBox boite_expandedBackgrounds_ ; // Contient le text "--------------- Expanded Backgrounds-------------------"

       Gtk::HBox boite14 ; 
       Gtk::VBox boite141 ; // contient les  Allies
       Gtk::HBox boite1411 ; // contient les    alliesLabel
       Gtk::HBox boite1412 ; // contient les    alliesEntry
       Gtk::HBox boite1413 ;//contactsLabel
       Gtk::HBox boite1414 ; 
       Gtk::HBox boite1415 ;
       Gtk::HBox boite1416 ;
       Gtk::HBox boite1417 ;
       Gtk::HBox boite1418 ;
       Gtk::HBox boite1419 ;
       Gtk::HBox boite1410 ;

       Gtk::VBox boite142 ;  
       Gtk::HBox boite1421 ; // contient les    mentorLabel
       Gtk::HBox boite1422 ; // contient les    mentorEntry
       Gtk::HBox boite1423 ; // resourcesLabel
       Gtk::HBox boite1424 ; // resourcesEntry
       Gtk::HBox boite1425 ;
       Gtk::HBox boite1426 ;
       Gtk::HBox boite1427 ;
       Gtk::HBox boite1428 ;
       Gtk::HBox boite1429 ;
       Gtk::HBox boite1420 ;

/* ********************************** */
        //Possessions
         Gtk::HBox boite_possessions_ ; // Contient le text "--------------- Possessions-------------------"

        Gtk::HBox boite15 ; 

        Gtk::VBox boite151 ;
        Gtk::HBox boite1511 ; // contient les    gearLabel
        Gtk::HBox boite1512 ; // contient les    gearEntry
        Gtk::HBox boite1513 ; // contient les    feedingGroundsLabel
        Gtk::HBox boite1514 ; //feedingGroundsEntry

        Gtk::VBox boite152 ;    
        Gtk::HBox boite1521 ;   //equipmentLabel
        Gtk::HBox boite1522 ; // contient les    equipmentEntry
        Gtk::HBox boite1523 ; // contient les    vehiclesLabel
        Gtk::HBox boite1524 ; //vehiclesLEntry

/* ********************************** */
        //bloodBondsVinculi
         Gtk::HBox boite_bloodBondsVinculi_ ; // Contient le text "--------------- bloodBondsVinculi-------------------"

       Gtk::HBox boite16 ; 

       Gtk::VBox boite161 ;
       Gtk::HBox boite1611 ; // contient les    ratingLabelcol1 et boundToLabelcol1
       Gtk::HBox boite1612 ; // contient les    ratinglisteDeroulantecol1 et boundToEntrycol1

       Gtk::VBox boite162 ;
       Gtk::HBox boite1621 ; 
       Gtk::HBox boite1622 ; 

/* ********************************** */
        //Havens
        Gtk::HBox boite_havens_ ; // Contient le text "--------------- Havens-------------------"

       Gtk::HBox boite17 ; 

       Gtk::VBox boite171 ;
       Gtk::HBox boite1711 ; // contient les    LocationLabel
       Gtk::HBox boite1712 ; // contient les    LocationEntry

       Gtk::VBox boite172 ;
       Gtk::HBox boite1721 ; // contient les    descritionLabel
       Gtk::HBox boite1722 ; // contient les    descrptionEntry

/* ********************************** */
        //History
        Gtk::HBox boite_history_ ; // Contient le text "--------------- History-------------------"

       Gtk::HBox boite18 ; 

       Gtk::VBox boite181 ;
       Gtk::HBox boite1811 ; // contient les    preludeLabel
       Gtk::HBox boite1812 ; // contient les    preludeEntry

       Gtk::VBox boite1813 ; // contient les    goalsLabel
       Gtk::HBox boite1814 ; // contient les    goalsLabel


/* ********************************** */
        //Description
        Gtk::HBox boite_description_ ; // Contient le text "--------------- Description -------------------"

       Gtk::HBox boite19 ; 
    //Alternance de Label et Entry des differents type de Descrption
       Gtk::VBox boite191 ;
       Gtk::HBox boite1911 ; // contient les    AgeLabel
       Gtk::HBox boite1912 ; // contient les    AgeEntry
       Gtk::HBox boite1913 ;
       Gtk::HBox boite1914 ;
       Gtk::HBox boite1915 ;
       Gtk::HBox boite1916 ;
       Gtk::HBox boite1917 ;
       Gtk::HBox boite1918 ;
       Gtk::HBox boite1919 ;
       Gtk::HBox boite1910 ;
       Gtk::HBox boite19101 ;
        
        //meme process mais pour Descrption descrption 
       Gtk::VBox boite192 ;
       Gtk::HBox boite1921 ;
       Gtk::HBox boite1922 ;
       Gtk::HBox boite1923 ;
       Gtk::HBox boite1924 ;
       Gtk::HBox boite1925 ;
       Gtk::HBox boite1926 ;
       Gtk::HBox boite1927 ;
       Gtk::HBox boite1928 ;
       Gtk::HBox boite1929 ;
       Gtk::HBox boite1920 ;
       Gtk::HBox boite19201 ;

/* ********************************** */
    //PARTIE Visuals
       Gtk::HBox boite_visuals_ ; //  Contient le text "--------------- Visuals -------------------"
       Gtk::HBox boite2011 ; // contient les    coterieChartLabel et characterSketchAgeLabel

       Gtk::HBox boite201 ;

       Gtk::VBox boite20 ;


       Gtk::HButtonBox boitePiedPage ; // bouton pour valider et quitter


/* ********************************** */
/* ********************************** */
       //PARTIE LABEL et des chaps a saisir ou liste déroulante
       //Gtk::Image imageTitre ;
       Gtk::Label titre11;  // "  20th ANNIVERSARY EDITION "
       Gtk::Label titre12;  // " VAMPIRE "
       Gtk::Label titre13;  // " THE MASQUERADE "

       Gtk::Label nameLabel;  // "  Name "
       Gtk::Entry nameEntry ; // Zone de saisis du nom du joueur

       Gtk::Label    listeDeroulanteClanLabel;  // "  Clan  "
       Gtk::ComboBoxText listeDeroulanteClan ; // Liste deroulante de la liste du clan du fichier "VampireV5.txt"

       Gtk::Label    listeDeroulanteNatureLabel;  // "  nature  "
       Gtk::ComboBoxText listeDeroulanteNature ; // Liste deroulante de la liste des natures  du fichier "VampireV5.txt"

       Gtk::Label playerLabel;  // "  Player "
       Gtk::Entry playerEntry ; // Zone de saisis du nom du player

       Gtk::Label demeanerLabel;  // "  demeaner "
       Gtk::Entry demeanerEntry ; // Zone de saisis du nom du demeaner

       Gtk::Label chronideLabel;  // "  chronide "
       Gtk::Entry chronideEntry ; // Zone de saisis du nom du chronide

       Gtk::Label generationLabel;  // "  generation "
       Gtk::Entry generationEntry ; // Zone de saisis du nom du generation

       Gtk::Label conceptLabel;  // "  concept "
       Gtk::Entry conceptEntry ; // Zone de saisis du nom du concept

       Gtk::Label sireLabel;  // "  sire "
       Gtk::Entry sireEntry ; // Zone de saisis du nom du sire

       Gtk::Label pointsLabel; // "Attributes: 7/5/3 • Abilities:13/9/5 • Disciplines:3 • Backgrounds:5 • Virtues:7"


/* ********************************** */
      // PARTIE ATTRIBUT
       Gtk::Label attributLabel;  // "  sire "

       Gtk::Label attributPhysique;  // "  Attribut Physique "
       Gtk::Label attributSocial;  // "  Attribut Social "
       Gtk::Label attributMental;  // "  Attribut Mental "

        //physique
       Gtk::Label strengthLabel;  // "  Strength____"
       Gtk::ComboBoxText strengthlisteDeroulante ; // information en lien a la force dans une liste déroulante
       Gtk::Label dexterityLabel;  // "  Dexterety___"
       Gtk::ComboBoxText dexteritylisteDeroulante ;// information en lien a la dexterité dans une liste déroulante
       Gtk::Label staminaLabel;  // "  Stamina"
       Gtk::ComboBoxText staminalisteDeroulante ;// information en lien a l'endurance dans une liste déroulante

        //Social
       Gtk::Label charismaLabel;  // "  Charisma"
       Gtk::ComboBoxText charismalisteDeroulante ;
       Gtk::Label manipulationLabel;  // "  Manipulation"
       Gtk::ComboBoxText manipulationlisteDeroulante ;
       Gtk::Label apprearanceLabel;  // "  appearance"
       Gtk::ComboBoxText apprearancelisteDeroulante ;

        //Mental
       Gtk::Label perceptionLabel;  // "  Perception "
       Gtk::ComboBoxText perceptionlisteDeroulante ;
       Gtk::Label intelligenceLabel;  // "  Intelligence"
       Gtk::ComboBoxText intelligencelisteDeroulante ;
       Gtk::Label witsLabel;  // "  Wits"
       Gtk::ComboBoxText witslisteDeroulante ;


/* ********************************** */
      // PARTIE Abilities
       Gtk::Label abilitiesLabel;  // "  abilities____ "

       Gtk::Label abilitiesTalents;  // "  abilities Talents "
       Gtk::Label abilitiesSkills;  // "  abilities Skills "
       Gtk::Label abilitiesKnowledges;  // "  abilities Knowledges "

        //Talents
       Gtk::Label alertnessLabel;  // "  alertness_____"
       Gtk::Entry alertnessEntry ; // information en lien a la force dans une liste déroulante
       Gtk::Label athleticsLabel;  // "  Athletics_____"
       Gtk::Entry athleticsEntry ;
       Gtk::Label awarenessLabel;  // "  awareness_____"
       Gtk::Entry awarenessEntry ;
       Gtk::Label brawlLabel;  // "  Brawl"
       Gtk::Entry brawlEntry ;
       Gtk::Label empathyLabel;  // "  empathy"
       Gtk::Entry empathyEntry ;
       Gtk::Label expressionLabel;  // "  expression"
       Gtk::Entry expressionEntry ;
       Gtk::Label intimidationLabel;  // "  Brawl"
       Gtk::Entry intimidationEntry ;
       Gtk::Label leadershipLabel;  // "  leadership"
       Gtk::Entry leadershipEntry ;
       Gtk::Label streetwiseLabel;  // "  streetwise"
       Gtk::Entry streetwiseEntry ;
       Gtk::Label subterfugeLabel;  // "  subterfuge"
       Gtk::Entry subterfugeEntry ;
       Gtk::Label hobbyTalentLabel;  // "  hobbyTalent"
       Gtk::Entry hobbyTalentEntry ;

       // Skills
       Gtk::Label animalkenLabel;  // "  Animal Ken_____"
       Gtk::Entry animalkenEntry ;
       Gtk::Label craftsLabel;  // "  Crafts_____"
       Gtk::Entry craftsEntry ;
       Gtk::Label driveLabel;  // "  Drive_____"
       Gtk::Entry driveEntry ;
       Gtk::Label etiquetteLabel;  // "  Etiquette_____"
       Gtk::Entry etiquetteEntry ;
       Gtk::Label firearmsLabel;  // "  Firearms_____"
       Gtk::Entry firearmsEntry ;
       Gtk::Label larcenyLabel;  // "  Larceny_____"
       Gtk::Entry larcenyEntry ;
       Gtk::Label meleeLabel;  // "  Melee_____"
       Gtk::Entry meleeEntry ;
       Gtk::Label performanceLabel;  // "  Performance_____"
       Gtk::Entry performanceEntry ;
       Gtk::Label stealthLabel;  // "  Stealth_____"
       Gtk::Entry stealthEntry ;
       Gtk::Label survivalLabel;  // "  Survival_____"
       Gtk::Entry survivalEntry ;
       Gtk::Label professionalSkillLabel;  // "  ProfessionalSkill_____"
       Gtk::Entry professionalSkillEntry ;


       //Knowledges
       Gtk::Label academicsLabel;  // "  Academics_____"
       Gtk::Entry academicsEntry ;
       Gtk::Label computerLabel;  // "  Computer_____"
       Gtk::Entry computerEntry ;
       Gtk::Label financeLabel;  // "  Finance_____"
       Gtk::Entry financeEntry ;
       Gtk::Label investigationLabel;  // "  investigationLabel_____"
       Gtk::Entry investigationEntry ;
       Gtk::Label lawLabel;  // "  Law_____"
       Gtk::Entry lawEntry ;
       Gtk::Label medicineLabel;  // "  Medicine_____"
       Gtk::Entry medicineEntry ;
       Gtk::Label occultLabel;  // "  Occult_____"
       Gtk::Entry occultEntry ;
       Gtk::Label politicsLabel;  // "  Politics_____"
       Gtk::Entry politicsEntry ;
       Gtk::Label scienceLabel;  // "  Science_____"
       Gtk::Entry scienceEntry ;
       Gtk::Label technologyLabel;  // "  Technology_____"
       Gtk::Entry technologyEntry ;
       Gtk::Label expertLabel;  // "  Expert_____"
       Gtk::Entry expertEntry ;

/* ********************************** */

       Gtk::Label advantageLabel;  // "  Advantage "
       
       Gtk::Label advantageDiscipline ;  // "  Attribut Discipline  "
       Gtk::Label advantageBackground;  // "  Attribut Background "
       Gtk::Label advantageVirtue;  // "  Attribut Virtue "

        //Discpline  liste deroulante
       Gtk::ComboBoxText discipline1listeDeroulante ;
       Gtk::ComboBoxText discipline2listeDeroulante ;
       Gtk::ComboBoxText discipline3listeDeroulante ;
       Gtk::ComboBoxText discipline4listeDeroulante ;
       Gtk::ComboBoxText discipline5listeDeroulante ;
       Gtk::ComboBoxText discipline6listeDeroulante ;

        //Discpline Valeur saisir du texte
       Gtk::Entry discipline1AdvantageVal ;
       Gtk::Entry discipline2AdvantageVal ;
       Gtk::Entry discipline3AdvantageVal ;
       Gtk::Entry discipline4AdvantageVal ;
       Gtk::Entry discipline5AdvantageVal ;
       Gtk::Entry discipline6AdvantageVal ;

        //Background  lise deroulante
       Gtk::ComboBoxText backgroud1listeDeroulante ;
       Gtk::ComboBoxText backgroud2listeDeroulante ;
       Gtk::ComboBoxText backgroud3listeDeroulante ;
       Gtk::ComboBoxText backgroud4listeDeroulante ;
       Gtk::ComboBoxText backgroud5listeDeroulante ;
       Gtk::ComboBoxText backgroud6listeDeroulante ;


        //Background Valeur saisir du texte
       Gtk::Entry backgroud1AdvantageVal ;
       Gtk::Entry backgroud2AdvantageVal ;
       Gtk::Entry backgroud3AdvantageVal ;
       Gtk::Entry backgroud4AdvantageVal ;
       Gtk::Entry backgroud5AdvantageVal ;
       Gtk::Entry backgroud6AdvantageVal ;

        //Background Vertue
       Gtk::Label virtue1Label ;
       Gtk::Entry virtue1 ;
       Gtk::Label virtue2Label ;
       Gtk::Entry virtue2 ;
       Gtk::Label virtue3Label ;
       Gtk::Entry virtue3 ;

/* ********************************** */
    //la parte apres Advantage
        Gtk::Label transitionLabel;  // "  -------"
        Gtk::Label tapeHereLabel;  // "  [Type Here ]"
        Gtk::Entry typeHereEntry;

    //Humanity/Path Willpower et tout

        Gtk::Label humanityPathLabel;  // "  --HumanityPath-----"
        Gtk:: Entry humanityPathEntry;

        Gtk::Label willpowerLabel;  // "  --Willpower-----"
        Gtk:: Entry willpowerEntry;

        Gtk::Label bloodPoolLabel;  // "  --Blood Pool-----"
        Gtk:: Entry bloodPoolEntry;

     //Health  Weakness Experience
        Gtk::Label healthPathLabel;  // "  --Health-----"
        Gtk::Label bruisedLabel;  // Health Bruised
        Gtk::Label hurtLabel;  // Health Hurt
        Gtk::Label injuredLabel;  // Health Injured
        Gtk::Label woundedLabel;  // Health Wounded
        Gtk::Label mauledLabel;  // Health Mauled
        Gtk::Label crippledLabel;  // Health Crippled
        Gtk::Label incapacitatedLabel;  // Health Incapacitated

        //meme chose pour les  listeDeroulante;
        Gtk::ComboBoxText healthlisteDeroulanteBruised;
        Gtk::ComboBoxText healthlisteDeroulanteHurt;
        Gtk::ComboBoxText healthlisteDeroulanteInjured;
        Gtk::ComboBoxText healthlisteDeroulanteWounded;
        Gtk::ComboBoxText healthlisteDeroulanteMauled;
        Gtk::ComboBoxText healthlisteDeroulanteCrippled;
        Gtk::ComboBoxText healthlisteDeroulanteIncapacitated;



        Gtk::Label weaknessLabel;  // "  --Weakness-----"
        Gtk:: Entry weaknessEntry;

        Gtk::Label experienceLabel;  // "  --Experience----"
        Gtk:: Entry experienceEntry;

/* ********************************** */
    //Merit and flaws

       //tout les label necessaire 
       Gtk::Label meritANDflawLabel ;
       Gtk::Label MeritLabel;
       Gtk::Label meritTypeLabel;
       Gtk::Label CostMeritLabel;
       Gtk::Label FlawLabel;
       Gtk::Label flawTypeLabel;
       Gtk::Label BonusLabel;

        //Merits loste deroulante
       Gtk::ComboBoxText merit1listeDeroulante ;
       Gtk::ComboBoxText merit2listeDeroulante ;
       Gtk::ComboBoxText merit3listeDeroulante ;
       Gtk::ComboBoxText merit4listeDeroulante ;
       Gtk::ComboBoxText merit5listeDeroulante ;
       Gtk::ComboBoxText merit6listeDeroulante ;
       Gtk::ComboBoxText merit7listeDeroulante ;

        //Merits Type liste deroulante
       Gtk::ComboBoxText meritType1listeDeroulante ;
       Gtk::ComboBoxText meritType2listeDeroulante ;
       Gtk::ComboBoxText meritType3listeDeroulante ;
       Gtk::ComboBoxText meritType4listeDeroulante ;
       Gtk::ComboBoxText meritType5listeDeroulante ;
       Gtk::ComboBoxText meritType6listeDeroulante ;
       Gtk::ComboBoxText meritType7listeDeroulante ;

        //Merits Cost liste deroulante
       Gtk::ComboBoxText meritCost1listeDeroulante ;
       Gtk::ComboBoxText meritCost2listeDeroulante ;
       Gtk::ComboBoxText meritCost3listeDeroulante ;
       Gtk::ComboBoxText meritCost4listeDeroulante ;
       Gtk::ComboBoxText meritCost5listeDeroulante ;
       Gtk::ComboBoxText meritCost6listeDeroulante ;
       Gtk::ComboBoxText meritCost7listeDeroulante ;

        //Flaws liste deroulante
       Gtk::ComboBoxText flow1listeDeroulante ;
       Gtk::ComboBoxText flow2listeDeroulante ;
       Gtk::ComboBoxText flow3listeDeroulante ;
       Gtk::ComboBoxText flow4listeDeroulante ;
       Gtk::ComboBoxText flow5listeDeroulante ;
       Gtk::ComboBoxText flow6listeDeroulante ;
       Gtk::ComboBoxText flow7listeDeroulante ;

        //Flaws Type liste deroulante
       Gtk::ComboBoxText flowType1listeDeroulante ;
       Gtk::ComboBoxText flowType2listeDeroulante ;
       Gtk::ComboBoxText flowType3listeDeroulante ;
       Gtk::ComboBoxText flowType4listeDeroulante ;
       Gtk::ComboBoxText flowType5listeDeroulante ;
       Gtk::ComboBoxText flowType6listeDeroulante ;
       Gtk::ComboBoxText flowType7listeDeroulante ;

        //Flaws Bonus liste deroulante
       Gtk::ComboBoxText bonus1listeDeroulante ;
       Gtk::ComboBoxText bonus2listeDeroulante ;
       Gtk::ComboBoxText bonus3listeDeroulante ;
       Gtk::ComboBoxText bonus4listeDeroulante ;
       Gtk::ComboBoxText bonus5listeDeroulante ;
       Gtk::ComboBoxText bonus6listeDeroulante ;
       Gtk::ComboBoxText bonus7listeDeroulante ;

/* ********************************** */
        //PARTIE Other Traits
        Gtk:: Label otherTraitsLabel;

        Gtk:: Entry otherTraits1Entry;
        Gtk:: Entry otherTraits2Entry;
        Gtk:: Entry otherTraits3Entry;

        Gtk:: ComboBoxText listeDeroulanteOtherTraits1;
        Gtk:: ComboBoxText listeDeroulanteOtherTraits2;
        Gtk:: ComboBoxText listeDeroulanteOtherTraits3;


/* ********************************** */
        //Rituals et Paths
        Gtk:: Label ritualsPathsLabel;

        //Ritual
        Gtk:: Label ritualsNameLabel;
        Gtk:: Label ritualsLevelLabel;
        Gtk:: ComboBoxText listeDeroulanteritualsName;
        Gtk:: ComboBoxText listeDeroulanteritualsLevel;

        //Path
        Gtk:: Label pathsNameLabel;
        Gtk:: ComboBoxText listeDeroulantepathsName;
        Gtk:: ComboBoxText listeDeroulantepathsLevel;

/* ********************************** */
        //PARTIE Experience  Derangements

        Gtk:: Label experienceDerangementsLabel;
        Gtk:: Label experienceDLabel;
        Gtk:: Label totalLabel;
        Gtk:: Label totalSpentLabel;
        Gtk:: Label spentOnLabel;
        Gtk:: Label derangementsLabel;

        Gtk:: Entry totalEntry;
        Gtk:: Entry totalSpentEntry;
        Gtk:: Entry spentOnEntry;
        Gtk:: Entry derangementsEntry;

/* ********************************** */
        //PARTIE Combat
        Gtk:: Label combatLabel;

        //pseudo tableau
        Gtk:: Label combatWeaponAttackLabel;
        Gtk:: Entry combatWeaponAttackEntry;
        Gtk:: Label combatDiffLabel;
        Gtk:: Entry combatDiffEntry;
        Gtk:: Label combatDamageLabel;
        Gtk:: Entry combatDamageEntry;
        Gtk:: Label combatRangeLabel;
        Gtk:: Entry combatRangeEntry;
        Gtk:: Label combatRateLabel;
        Gtk:: Entry combatRateEntry;
        Gtk:: Label combatClipLabel;
        Gtk:: Entry combatClipEntry;
        Gtk:: Label combatConcealLabel;
        Gtk:: Entry combatConcealEntry;

        //armor
        Gtk:: Label armorArmorLabel;
        Gtk:: Label armorClassLabel;
        Gtk:: Entry armorClassEntry;
        Gtk:: Label armorRatingLabel;
        Gtk:: Entry armorRatingEntry;
        Gtk:: Label armorPenaltyLabel;
        Gtk:: Entry armorPenaltyEntry;
        Gtk:: Label armorDescriptionLabel;
        Gtk:: Entry armorDescriptionEntry;


/* ********************************** */
        //PARTIE Expanded Backgrounds

        Gtk:: Label expandedBackgroundsLabel;

        Gtk:: Label alliesLabel;
        Gtk:: Label mentorLabel;
        Gtk:: Label contactsLabel;
        Gtk:: Label resourcesLabel;
        Gtk:: Label fameLabel;
        Gtk:: Label retainersLabel;
        Gtk:: Label herdLabel;
        Gtk:: Label statusLabel;
        Gtk:: Label influenceLabel;
        Gtk:: Label otherLabel;

        Gtk:: Entry alliesEntry;
        Gtk:: Entry mentorEntry;
        Gtk:: Entry contactsEntry;
        Gtk:: Entry resourcesEntry;
        Gtk:: Entry fameEntry;
        Gtk:: Entry retainersEntry;
        Gtk:: Entry herdEntry;
        Gtk:: Entry statusEntry;
        Gtk:: Entry influenceEntry;
        Gtk:: ComboBoxText other1listeDeroulante;
        Gtk:: Entry other2Entry;

/* ********************************** */
        //PARTIE Possessions

        Gtk:: Label possessionsLabel;

        Gtk:: Label gearLabel;
        Gtk:: Label feedingGroundsLabel;
        Gtk:: Label equipmentLabel;
        Gtk:: Label vehiclesLabel;

        Gtk:: Entry gearEntry;
        Gtk:: Entry feedingGroundsEntry;
        Gtk:: Entry equipmentEntry;
        Gtk:: Entry vehiclesEntry;


/* ********************************** */
        //PARTIE Blood   Bonds/Vinculi

        Gtk:: Label bloodBondsVinculiLabel;

        Gtk:: Label boundToLabelcol1;
        Gtk:: Label ratingLabelcol1;
        Gtk:: Label boundToLabelcol2;
        Gtk:: Label ratingLabelcol2;

        Gtk:: Entry boundToEntrycol1;
        Gtk:: Entry boundToEntrycol2;
        Gtk:: ComboBoxText ratinglisteDeroulantecol2;
        Gtk:: ComboBoxText ratinglisteDeroulantecol1;

/* ********************************** */
        //Havens
        Gtk:: Label havensLabel;

        Gtk:: Label locationLabel;
        Gtk:: Label descriptionHavensLabel;

        Gtk:: Entry locationEntry;
        Gtk:: Entry descriptionHavensEntry;

/* ********************************** */
        //History
        Gtk:: Label historyLabel;

        Gtk:: Label preludeLabel;
        Gtk:: Label goalsLabel;

        Gtk:: Entry preludeEntry;
        Gtk:: Entry goalsEntry;

/* ********************************** */
        //Description
        Gtk:: Label descriptionDescriptionLabel;

        Gtk:: Label descriptionAgeLabel;
        Gtk:: Entry descriptionAgeEntry;

        Gtk:: Label descriptionApparentAgeLabel;
        Gtk:: Entry descriptionApparentAgeEntry;

        Gtk:: Label descriptionDateofBirthLabel;
        Gtk:: Entry descriptionDateofBirthEntry;

        Gtk:: Label descriptionRIPLabel;
        Gtk:: Entry descriptionRIPEntry;

        Gtk:: Label descriptionHairLabel;
        Gtk:: Entry descriptionHairEntry;

        Gtk:: Label descriptionEyesLabel;
        Gtk:: Entry descriptionEyesEntry;

        Gtk:: Label descriptionRaceLabel;
        Gtk:: Entry descriptionRaceEntry;

        Gtk:: Label descriptionNationalityLabel;
        Gtk:: Entry descriptionNationalityEntry;

        Gtk:: Label descriptionHeightLabel;
        Gtk:: Entry descriptionHeightEntry;

        Gtk:: Label descriptionWeightLabel;
        Gtk:: Entry descriptionWeightEntry;

        Gtk:: Label descriptionSexLabel;
        Gtk:: Entry descriptionSexEntry;

        //la partie situer dans la colone 2 (descrption des descrption)

        Gtk:: Entry descriptionAgeEntry2;
        Gtk:: Entry descriptionApparentAgeEntry2;
        Gtk:: Entry descriptionDateofBirthEntry2;
        Gtk:: Entry descriptionRIPEntry2;
        Gtk:: Entry descriptionHairEntry2;
        Gtk:: Entry descriptionEyesEntry2;
        Gtk:: Entry descriptionRaceEntry2;
        Gtk:: Entry descriptionNationalityEntry2;
        Gtk:: Entry descriptionHeightEntry2;
        Gtk:: Entry descriptionWeightEntry2;
        Gtk:: Entry descriptionSexEntry2;

/* ********************************** */
        //Visuals
        Gtk:: Label visualsLabel;
        Gtk:: Label coterieChartLabel;
        Gtk:: Label characterSketchAgeLabel;



};


#endif // FENETRE_H_INCLUDED


