#ifndef VAMPIRE_H_INCLUDED
#define VAMPIRE_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>
//#include <gtkmm.h>
//#include <gtkmm/application.h>

using namespace std;


/* Cette classe memorize et manipule les donnees sur les clans de vampire */
class Clan
{
	protected:
		string clanName;						/// Nom du clan de votre vampire

		string nickname;						/// Surnom du clan
		string background;						/// Passe commun des membres de votre clan
		string sect;							/// Secte a laquelle les vampires de votre clan appartient habituellement
		string haven;							///
		string appearance;						///
		string character;
		int nbPointsDisciplines;					/// Nombre de points a distribuer dans les disciples
		vector <string> disciplines;					/// Tableau de nom des disciplines choisies
		vector <int> valueDisciples;					/// Tableau des points investits dans la disciplines choisi
		string weakness;
		string organization;

		string nature_and_demeanor;

	public:
//	    Clan() ;
//	    Clan(string clanName) ;  // Constructeur



		string GetClanName();
		void SetClanName(string);

		string GetNickname();
		void SetNickname(string);

		string GetBackground();
		void SetBackground(string);

		string GetSect();
		void SetSect(string);

		string GetHaven();
		void SetHaven(string);

		string GetAppearance();
		void SetAppearance(string);

		string GetCharacter();
		void SetCharacter(string);

		//discipline
		string GetDisciplinesName(int);
		int GetValueDisciplines(int);
		void SetInitDisciplines(string, string, string);
		void AddValueDisciplines(int, int);

		//weakness
		string GetWeakness();
		void SetWeakness(string);
		//organisation
		string GetOrganization();
		void SetOrganization(string);

		//Nature_and_Demeanor (j'ai l'impression que c'est pas toujours present ca)
		string GetNature_and_Demeanor();
		void SetNature_and_Demeanor(string);

};


//  Cette classe contient les tableaux des Clan, Nature, ...
class TableCategories
{
	protected:
        vector<string> tableauClan ;
        vector<string> tableauNature ;

		//atribut
		//physique
        vector<string> tableauStrength ;
        vector<string> tableauDexterity ;
        vector<string> tableauStamina ;
 vector<string> tableauCharisma ;
        vector<string> tableauManipulation ;
        vector<string> tableauAppearance ;
        vector<string> tableauPerception ;
        vector<string> tableauIntelligence ;
        vector<string> tableauWits ;
        vector<string> tableauDiscipline ;
        vector<string> tableauBackground ;
        vector<string> tableauMerit ;
        vector<string> tableauMeritType ;
        vector<string> tableauFlaw ;
		//HEATH
		//heath
		vector<string> tableauHealth;

		//RITUALS PATH
		//Ritual
        vector<string> tableauRitualLevel ;
        vector<string> tableauRitualName ;

		//Path
        vector<string> tableauPathLevel ;
        vector<string> tableauPathName ;


		//OTHER TRAIT
		vector<string> tableauOtherTraits;
	
		//Expanded Backgrounds
		vector<string> tableauOtherExpandedBackgrounds;

		//Blood   Bonds/Vinculi
        vector<string> tableauRating ;


		

	public:

        vector<string> GetchargerNomClan () ;
        void SetchargerNomClan (vector<string>) ;

        vector<string> GetchargerNature () ;
        void SetchargerNature(vector<string>) ;

		/****************************************/
		//PARTIE ATRIBUT
		//Physique
        vector<string> GettableauStrength  () ;
        void SettableauStrength  (vector<string>) ;

        vector<string> GetTableauDexterity () ;
        void SettableauDexterity (vector<string>) ;

        vector<string> GetTableauStamina () ;
        void SetTableauStamina (vector<string>) ;

		/****************************************/

		//Health tab 
		vector<string> GetTableauHealth () ;
        void SetTableauHealth () ; //pas besoin de parametre dans le set car le contenu du tableau ne va pas changer 

		/****************************************/
		//Rituals and Paths 
		vector<string> GetTableauRitualsLevel () ;
		vector<string> GetTableauPathLevel () ;
        void SetTableauRitualsPathLevel () ; 

		vector<string> GetTableauRitualsName () ;
        void SetTableauRitualsName (vector<string>) ; 

		vector<string> GetTableauPathName () ;
        void SetTableauPathName (vector<string>) ; 


		/****************************************/
		//Other Trait tab 
		vector<string> GetTableauOtherTraits () ;
        void SetTableauOtherTraits () ; 

		/****************************************/
		//Backgrounds pour Other pour Expanded Backgrounds 
		vector<string> GetTableauOtherExpandedBackgrounds () ;
        void SetTableauOtherExpandedBackgrounds (vector<string>) ; 

	
		/****************************************/
		//Blood   Bonds/Vinculi
		vector<string> GetTableauRating () ;
        void SetTableauRating () ; //pas besoin de parametre dans le set car le contenu du tableau ne va pas changer 

  vector<string> GetTableauCharisma () ;
        void SetTableauCharisma (vector<string>) ;

        vector<string> GetTableauManipulation () ;
        void SetTableauManipulation (vector<string>) ;

        vector<string> GetTableauAppearance () ;
        void SetTableauAppearance (vector<string>) ;

        vector<string> GetTableauPerception () ;
        void SetTableauPerception (vector<string>) ;

        vector<string> GetTableauIntelligence  () ;
        void SetTableauIntelligence  (vector<string>) ;

        vector<string> GetTableauWits () ;
        void SetTableauWits  (vector<string>) ;

        vector<string> GetTableauDiscipline () ;
        void SetTableauDiscipline  (vector<string>);

        vector<string> GetTableauBackground () ;
        void SetTableauBackground  (vector<string>);

        vector<string> GetTableauMerit () ;
        void SetTableauMerit (vector<string>);

        vector<string> GetTableauMeritType () ;
        void SetTableauMeritType (vector<string>);

        vector<string> GetTableauFlaw () ;
        void SetTableauFlaw(vector<string>);


};


TableCategories ChargerTableauxClanNatureetc() ;


//osf pour l'instant
vector<Clan> AlimenterClassClan() ;
Clan functionClan(Clan clan, string nomDuClan , string ligne, int  positionDebut,  int rangAttributClan) ;


#endif // VAMPIRE_H_INCLUDED
