#ifndef FENETRE_H_INCLUDED
#define FENETRE_H_INCLUDED

// on appele pas tout la librairie mais uniquement les parties qui nous interesse cela évite de charger la librairie en entier
#include <gtkmm/window.h>
#include <gtkmm/button.h>
#include <gtkmm/stock.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/image.h>
#include <gtkmm/entry.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/messagedialog.h>

#include <gtkmm/scrolledwindow.h>


//utilisation de string d'ou l'appelation de la librairie adéquate
#include <string>

using namespace std;


/***********************************************/
//Classe pour stocker la saisie
class VampireClass
{
    protected:


        //variables de type glib qui vont stocker le nom de l'utilisateur, le clan , nature ect...
        Glib::ustring nomUtilisateur;
        Glib::ustring nomClan;
        Glib::ustring nomNature;
        Glib::ustring player;
        Glib::ustring demeanor;
        Glib::ustring chronide;
        Glib::ustring generation;
        Glib::ustring concept;
        Glib::ustring sire;


        /**********************************/
        //partie ATTRIBUT
        //variables de type glib qui vont stocker information sur les attribut
        //PHYSIQUE
        Glib::ustring strengthAttribut;
        Glib::ustring dexterityAttribut;
        Glib::ustring staminaAttribut;

        //SOCIAL
        Glib::ustring charismaAttribut;
        Glib::ustring manipulationAttribut;
        Glib::ustring appearanceAttribut;

        //MENTAL
        Glib::ustring perceptionAttribut;
        Glib::ustring intelligenceAttribut;
        Glib::ustring witsAttribut;

        /**********************************/
        //partie Abilities
        //variables de type glib qui vont stocker information sur les Abilities
        //TALENT
        Glib::ustring alertnessAbilities;
        Glib::ustring athleticsAbilities;
        Glib::ustring awarenessAbilities;
        Glib::ustring brawlAbilities;
        Glib::ustring empathyAbilities;
        Glib::ustring expressionAbilities;
        Glib::ustring intimidationAbilities;
        Glib::ustring leadershipAbilities;
        Glib::ustring streetwiseAbilities;
        Glib::ustring subterfugeAbilities;
        Glib::ustring hobbyTalentAbilities;


        //SKILLS
        Glib::ustring animalkenAbilities;
        Glib::ustring craftsAbilities;
        Glib::ustring driveAbilities;
        Glib::ustring etiquetteAbilities;
        Glib::ustring firearmsAbilities;
        Glib::ustring larcenyAbilities;
        Glib::ustring meleeAbilities;
        Glib::ustring performanceAbilities;
        Glib::ustring stealthAbilities;
        Glib::ustring survivalAbilities;
        Glib::ustring professionalSkillAbilities;

        //KNOWNLEDGES
        Glib::ustring academicsAbilities;
        Glib::ustring computerAbilities;
        Glib::ustring financeAbilities;
        Glib::ustring investigationAbilities;
        Glib::ustring lawAbilities;
        Glib::ustring medicineAbilities;
        Glib::ustring occultAbilities;
        Glib::ustring politicsAbilities;
        Glib::ustring scienceAbilities;
        Glib::ustring technologyAbilities;
        Glib::ustring expertAbilities;

        /**********************************/
        //partie advantage
        //variables de type glib qui vont stocker information sur les advantage
        //Discipline
        Glib::ustring discipline1Advantage ;
        Glib::ustring discipline2Advantage ;
        Glib::ustring discipline3Advantage ;
        Glib::ustring discipline4Advantage ;
        Glib::ustring discipline5Advantage ;
        Glib::ustring discipline6Advantage ;

        //Discipline valeur
        Glib::ustring discipline1AdvantageVal ;
        Glib::ustring discipline2AdvantageVal ;
        Glib::ustring discipline3AdvantageVal ;
        Glib::ustring discipline4AdvantageVal ;
        Glib::ustring discipline5AdvantageVal ;
        Glib::ustring discipline6AdvantageVal ;

        //background
        Glib::ustring background1Advantage ;
        Glib::ustring background2Advantage ;
        Glib::ustring background3Advantage ;
        Glib::ustring background4Advantage ;
        Glib::ustring background5Advantage ;
        Glib::ustring background6Advantage ;

        //Discipline background
        Glib::ustring background1AdvantageVal ;
        Glib::ustring background2AdvantageVal ;
        Glib::ustring background3AdvantageVal ;
        Glib::ustring background4AdvantageVal ;
        Glib::ustring background5AdvantageVal ;
        Glib::ustring background6AdvantageVal ;

        //vertue (troisieme et derniere colonne )
        Glib::ustring virtue1 ;
        Glib::ustring virtue2 ;
        Glib::ustring virtue3 ;


        /**********************************/
        //Partie en dessous de Advantage
        //HEALTH
        //obliger de declarer des variable differente a chaque fois car info qu'on a stocket dans le ficher n'est jamais la meme
        Glib::ustring healthBruised;
        Glib::ustring healthHurt;
        Glib::ustring healthInjured;
        Glib::ustring healthWounded;
        Glib::ustring healthMauled;
        Glib::ustring healthCrippled;
        Glib::ustring healthIncapacitated;

        //Option a coté de HEALTH
        Glib::ustring typeHere;
        Glib::ustring humanityPath;
        Glib::ustring willpower;
        Glib::ustring bloodPool;
        Glib::ustring weakness;
        Glib::ustring experience;



        /**********************************/

        //Merit
        Glib::ustring merit1 ;
        Glib::ustring merit2 ;
        Glib::ustring merit3 ;
        Glib::ustring merit4 ;
        Glib::ustring merit5 ;
        Glib::ustring merit6 ;
        Glib::ustring merit7 ;

        //Merit Type     
        Glib::ustring meritType1 ;
        Glib::ustring meritType2 ;
        Glib::ustring meritType3 ;
        Glib::ustring meritType4 ;
        Glib::ustring meritType5 ;
        Glib::ustring meritType6 ;
        Glib::ustring meritType7 ;

        //Merit Cost     
        Glib::ustring meritCost1 ;
        Glib::ustring meritCost2 ;
        Glib::ustring meritCost3 ;
        Glib::ustring meritCost4 ;
        Glib::ustring meritCost5 ;
        Glib::ustring meritCost6 ;
        Glib::ustring meritCost7 ;

        //Flaw     
        Glib::ustring flaw1 ;
        Glib::ustring flaw2 ;
        Glib::ustring flaw3 ;
        Glib::ustring flaw4 ;
        Glib::ustring flaw5 ;
        Glib::ustring flaw6 ;
        Glib::ustring flaw7 ;

        //Flaw Type     
        Glib::ustring flawType1 ;
        Glib::ustring flawType2 ;
        Glib::ustring flawType3 ;
        Glib::ustring flawType4 ;
        Glib::ustring flawType5 ;
        Glib::ustring flawType6 ;
        Glib::ustring flawType7 ;

        //Flaw bonus     
        Glib::ustring flawBonus1 ;
        Glib::ustring flawBonus2 ;
        Glib::ustring flawBonus3 ;
        Glib::ustring flawBonus4 ;
        Glib::ustring flawBonus5 ;
        Glib::ustring flawBonus6 ;
        Glib::ustring flawBonus7 ;

        /**********************************/
        //partie  Other Traits
        Glib::ustring otherTraits1Name;
        Glib::ustring otherTraits2Name;
        Glib::ustring otherTraits3Name;

        //Valeur
        Glib::ustring otherTraits1Value;
        Glib::ustring otherTraits2Value;
        Glib::ustring otherTraits3Value;

       
        /**********************************/
        //partie  Rituals Path
        Glib::ustring ritualsName;
        Glib::ustring ritualsLevel;
        //Path
        Glib::ustring pathsName;
        Glib::ustring pathsLevel;


        /**********************************/
        //Experience  Derangements
        Glib::ustring total;
        Glib::ustring totalSpent;
        Glib::ustring spentOn;
        Glib::ustring derangements;

        /**********************************/
        //Combat
        //Tableau combat
        Glib::ustring combatWeaponAttack;
        Glib::ustring combatDiff;
        Glib::ustring combatDamage;
        Glib::ustring combatRange;
        Glib::ustring combatRate;
        Glib::ustring combatClip;
        Glib::ustring combatConceal;

        //Armor
        Glib::ustring armorClass;
        Glib::ustring armorRating;
        Glib::ustring armorPenalty;
        Glib::ustring armorDescription;

        /**********************************/
        //Expanded Backgrounds
        Glib::ustring allies;
        Glib::ustring contacts;
        Glib::ustring fame;
        Glib::ustring herd;
        Glib::ustring influence;
        Glib::ustring mentor;
        Glib::ustring resources;
        Glib::ustring retainers;
        Glib::ustring status;
        Glib::ustring other1;
        Glib::ustring other2;

        /**********************************/
        //Possessions
        Glib::ustring gear;
        Glib::ustring feedingGrounds;
        Glib::ustring equipment;
        Glib::ustring vehicles;


        /**********************************/
        //Blood   Bonds/Vinculi
        Glib::ustring boundToCol1;
        Glib::ustring boundToCol2;
        Glib::ustring ratingcol1;
        Glib::ustring ratingcol2;

        /**********************************/
        // Havens
        Glib::ustring location;
        Glib::ustring descriptionHavens;

        /**********************************/
        // History
        Glib::ustring prelude;
        Glib::ustring goals;


       /**********************************/
        //Expanded Backgrounds
       Glib::ustring descriptionAge;
       Glib::ustring descriptionApparentAge;
       Glib::ustring descriptionDateofBirth;
       Glib::ustring descriptionRIP;
       Glib::ustring descriptionHair;
       Glib::ustring descriptionEyes;
       Glib::ustring descriptionRace;
       Glib::ustring descriptionNationality;
       Glib::ustring descriptionHeight;
       Glib::ustring descriptionWeight;
       Glib::ustring descriptionSex;

        //2 eme colonne Description
       Glib::ustring descriptionAgeCol2;
       Glib::ustring descriptionApparentAgeCol2;
       Glib::ustring descriptionDateofBirthCol2;
       Glib::ustring descriptionRIPCol2;
       Glib::ustring descriptionHairCol2;
       Glib::ustring descriptionEyesCol2;
       Glib::ustring descriptionRaceCol2;
       Glib::ustring descriptionNationalityCol2;
       Glib::ustring descriptionHeightCol2;
       Glib::ustring descriptionWeightCol2;
       Glib::ustring descriptionSexCol2;

/* ********************************** */
/* ********************************** */


    public:
        //prototype des set et get 

        //getter de type Glib donc retourne une variable de type Glib
        Glib::ustring GetSire();
        //setter ne retourne vient car la fonciton est void
		void SetSire(Glib::ustring p_Sire);

		//meme principe pour le reste des parametres (notion de getter et setter )
        Glib::ustring GetConcept();
		void SetConcept(Glib::ustring p_Concept);

		Glib::ustring GetGeneration();
		void SetGeneration(Glib::ustring p_Generation);

		Glib::ustring GetChronide();
		void SetChronide(Glib::ustring p_Chronide);

		Glib::ustring GetDemeanor();
		void SetDemeanor(Glib::ustring p_Demeanor);

        Glib::ustring GetPlayer();
		void SetPlayer(Glib::ustring p_Player);

		Glib::ustring GetNomUtilisateur();
		void SetNomUtilisateur(Glib::ustring p_nomUtilisateur);

		Glib::ustring GetNomClan();
		void SetNomClan(Glib::ustring p_nomNatureClan);

		Glib::ustring GetNomNature();
		void SetNomNature(Glib::ustring p_nomNature);

/* ********************************** */
        //ATTRIBUT
        //physique
		Glib::ustring GetStrengthAttribut();
		void SetStrengthAttribut(Glib::ustring p_strengthAttribut);

		Glib::ustring GetDexterityAttribut();
		void SetDexterityAttribut(Glib::ustring p_dexterityAttribut);

		Glib::ustring GetStaminaAttribut();
		void SetStaminaAttribut(Glib::ustring p_staminaAttribut);

        //Social
        Glib::ustring GetCharismaAttribut();
		void SetCharismaAttribut(Glib::ustring p_charismaAttribut);

		Glib::ustring GetManipulationAttribut();
		void SetManipulationAttribut(Glib::ustring p_manipulationAttribut);

		Glib::ustring GetAppearanceAttribut();
		void SetAppearanceAttribut(Glib::ustring p_appearanceAttribut);

        //Mental
		Glib::ustring GetPerceptionAttribut();
		void SetPerceptionAttribut(Glib::ustring p_perceptionAttribut);

		Glib::ustring GetIntelligenceAttribut();
		void SetIntelligenceAttribut(Glib::ustring p_intelligenceAttribut);

		Glib::ustring GetWitsAttribut();
		void SetWitsAttribut(Glib::ustring p_witsAttribut);

/* ********************************** */
        //ABILITIES
        //Talent
		Glib::ustring GetAlertnessAbilities();
		void SetAlertnessAbilities(Glib::ustring p_alertnessAbilities);

        Glib::ustring GetAthleticsAbilities();
		void SetAthleticsAbilities(Glib::ustring p_athleticsAbilities);

        Glib::ustring GetAwarenessAbilities();
		void SetAwarenessAbilities(Glib::ustring p_awarenessAbilities);

        Glib::ustring GetBrawlAbilities();
		void SetBrawlAbilities(Glib::ustring p_brawlAbilities);

        Glib::ustring GetEmpathyAbilities();
		void SetEmpathyAbilities(Glib::ustring p_empathyAbilities);

        Glib::ustring GetExpressionAbilities();
		void SetExpressionAbilities(Glib::ustring p_expressionAbilities);

        Glib::ustring GetIntimidationAbilities();
		void SetIntimidationAbilities(Glib::ustring p_intimidationAbilities);

        Glib::ustring GetLeadershipAbilities();
		void SetLeadershipAbilities(Glib::ustring p_leadershipAbilities);

        Glib::ustring GetStreetwiseAbilities();
		void SetStreetwiseAbilities(Glib::ustring p_streetwiseAbilities);

        Glib::ustring GetSubterfugeAbilities();
		void SetSubterfugeAbilities(Glib::ustring p_subterfugeAbilities);

        Glib::ustring GetHobbyTalentAbilities();
		void SetHobbyTalentAbilities(Glib::ustring p_hobbyTalentAbilitiesAbilities);


        //skills
        Glib::ustring GetAnimalkenAbilities();
		void SetAnimalkenAbilities(Glib::ustring p_animalkenAbilities);

        Glib::ustring GetCraftsAbilities();
		void SetCraftsAbilities(Glib::ustring p_craftsAbilities);

        Glib::ustring GetDriveAbilities();
		void SetDriveAbilities(Glib::ustring p_driveAbilities);

        Glib::ustring GetEtiquetteAbilities();
		void SetEtiquetteAbilities(Glib::ustring p_etiquetteAbilities);

        Glib::ustring GetFirearmsAbilities();
		void SetFirearmsAbilities(Glib::ustring p_firearmsAbilities);

        Glib::ustring GetLarcenyAbilities();
		void SetLarcenyAbilities(Glib::ustring p_larcenyAbilities);

        Glib::ustring GetMeleeAbilities();
		void SetMeleeAbilities(Glib::ustring p_meleeAbilities);

        Glib::ustring GetPerformanceAbilities();
		void SetPerformanceAbilities(Glib::ustring p_performanceAbilities);

        Glib::ustring GetStealthAbilities();
		void SetStealthAbilities(Glib::ustring p_stealthAbilities);

        Glib::ustring GetSurvivalAbilities();
		void SetSurvivalAbilities(Glib::ustring p_survivalAbilities);

        Glib::ustring GetProfessionalSkillAbilities();
		void SetProfessionalSkillAbilities(Glib::ustring p_professionalSkillAbilities);


        //Knowledges
        Glib::ustring GetAcademicsAbilities();
		void SetAcademicsAbilities(Glib::ustring p_academicsAbilities);

        Glib::ustring GetComputerAbilities();
		void SetComputerAbilities(Glib::ustring p_computerAbilities);

        Glib::ustring GetFinanceAbilities();
		void SetFinanceAbilities(Glib::ustring p_financeAbilities);

        Glib::ustring GetInvestigationAbilities();
		void SetInvestigationAbilities(Glib::ustring p_investigationAbilities);

        Glib::ustring GetLawAbilities();
		void SetLawAbilities(Glib::ustring p_lawAbilities);

        Glib::ustring GetMedicineAbilities();
		void SetMedicineAbilities(Glib::ustring p_medicineAbilities);

        Glib::ustring GetOccultAbilities();
		void SetOccultAbilities(Glib::ustring p_occultAbilities);

        Glib::ustring GetPoliticsAbilities();
		void SetPoliticsAbilities(Glib::ustring p_politicsAbilities);

        Glib::ustring GetScienceAbilities();
		void SetScienceAbilities(Glib::ustring p_scienceAbilities);

        Glib::ustring GetTechnologyAbilities();
		void SetTechnologyAbilities(Glib::ustring p_technologyAbilities);

        Glib::ustring GetExpertAbilities();
		void SetExpertAbilities(Glib::ustring p_expertAbilities);


/* ********************************** */
        //Advantage

        //Discpline
		Glib::ustring GetDiscipline1Advantage();
		void SetDiscipline1Advantage(Glib::ustring p_discipline1Advantage);

        Glib::ustring GetDiscipline2Advantage();
		void SetDiscipline2Advantage(Glib::ustring p_discipline2Advantage);

		Glib::ustring GetDiscipline3Advantage();
		void SetDiscipline3Advantage(Glib::ustring p_discipline3Advantage);

		Glib::ustring GetDiscipline4Advantage();
		void SetDiscipline4Advantage(Glib::ustring p_discipline4Advantage);

		Glib::ustring GetDiscipline5Advantage();
		void SetDiscipline5Advantage(Glib::ustring p_discipline5Advantage);

		Glib::ustring GetDiscipline6Advantage();
		void SetDiscipline6Advantage(Glib::ustring p_discipline6Advantage);

		Glib::ustring GetDiscipline1AdvantageVal();
		void SetDiscipline1AdvantageVal(Glib::ustring p_discipline1AdvantageVal);

        Glib::ustring GetDiscipline2AdvantageVal();
		void SetDiscipline2AdvantageVal(Glib::ustring p_discipline2AdvantageVal);

		Glib::ustring GetDiscipline3AdvantageVal();
		void SetDiscipline3AdvantageVal(Glib::ustring p_discipline3AdvantageVal);

		Glib::ustring GetDiscipline4AdvantageVal();
		void SetDiscipline4AdvantageVal(Glib::ustring p_discipline4AdvantageVal);

		Glib::ustring GetDiscipline5AdvantageVal();
		void SetDiscipline5AdvantageVal(Glib::ustring p_discipline5AdvantageVal);

		Glib::ustring GetDiscipline6AdvantageVal();
		void SetDiscipline6AdvantageVal(Glib::ustring p_discipline6AdvantageVal);

        //Background
		Glib::ustring GetBackground1Advantage();
		void SetBackground1Advantage(Glib::ustring p_background1Advantage);

		Glib::ustring GetBackground2Advantage();
		void SetBackground2Advantage(Glib::ustring p_background2Advantage);

		Glib::ustring GetBackground3Advantage();
		void SetBackground3Advantage(Glib::ustring p_background3Advantage);

		Glib::ustring GetBackground4Advantage();
		void SetBackground4Advantage(Glib::ustring p_background4Advantage);

		Glib::ustring GetBackground5Advantage();
		void SetBackground5Advantage(Glib::ustring p_background5Advantage);

		Glib::ustring GetBackground6Advantage();
		void SetBackground6Advantage(Glib::ustring p_background6Advantage);

		Glib::ustring GetBackground1AdvantageVal();
		void SetBackground1AdvantageVal(Glib::ustring p_background1AdvantageVal);

        Glib::ustring GetBackground2AdvantageVal();
		void SetBackground2AdvantageVal(Glib::ustring p_background2AdvantageVal);

        Glib::ustring GetBackground3AdvantageVal();
		void SetBackground3AdvantageVal(Glib::ustring p_background3AdvantageVal);

        Glib::ustring GetBackground4AdvantageVal();
		void SetBackground4AdvantageVal(Glib::ustring p_background4AdvantageVal);

        Glib::ustring GetBackground5AdvantageVal();
		void SetBackground5AdvantageVal(Glib::ustring p_background5AdvantageVal);

        Glib::ustring GetBackground6AdvantageVal();
		void SetBackground6AdvantageVal(Glib::ustring p_background6AdvantageVal);

        //Vertue
		Glib::ustring GetVirtue1();
		void SetVirtue1(Glib::ustring p_virtue1);

		Glib::ustring GetVirtue2();
		void SetVirtue2(Glib::ustring p_virtue2);

		Glib::ustring GetVirtue3();
		void SetVirtue3(Glib::ustring p_virtue3);

/* ********************************** */
        //Partie en dessous de Advantage

        //HEALTH
        //obliger de declarer des variable differente a chaque fois car info qu'on a stocket dans le ficher n'est jamais la meme
        Glib::ustring GetHealthBruised();
		void SetHealthBruised(Glib::ustring p_HealthBruised);

        Glib::ustring GetHealthHurt();
		void SetHealthHurt(Glib::ustring p_HealthHurt);


        Glib::ustring GetHealthInjured();
		void SetHealthInjured(Glib::ustring p_HealthInjured);


        Glib::ustring GetHealthWounded();
		void SetHealthWounded(Glib::ustring p_HealthWounded);


        Glib::ustring GetHealth();
		void SetHealth(Glib::ustring p_Health);


        Glib::ustring GetHealthMauled();
		void SetHealthMauled(Glib::ustring p_HealthMauled);


        Glib::ustring GetHealthCrippled();
		void SetHealthCrippled(Glib::ustring p_HealthCrippled);


        Glib::ustring GetHealthIncapacitated();
		void SetHealthIncapacitated(Glib::ustring p_HealthIncapacitated);

        //option a cote de HEATLH
        Glib::ustring GetWeakness();
		void SetWeakness(Glib::ustring p_Weakness);

        Glib::ustring GetExperience();
		void SetExperience(Glib::ustring p_Experience);

        Glib::ustring GetHumanityPath();
		void SetHumanityPath(Glib::ustring p_HumanityPath);

        Glib::ustring GetWillpower();
		void SetWillpower(Glib::ustring p_Willpower);

        Glib::ustring GetBloodPool();
		void SetBloodPool(Glib::ustring p_BloodPool);

        Glib::ustring GetTypeHere();
		void SetTypeHere(Glib::ustring p_TypeHere);


/* ********************************** */
        //Merit & Flaws

        //Merits
		Glib::ustring GetMerit1();
		void SetMerit1(Glib::ustring p_merit1);

		Glib::ustring GetMerit2();
		void SetMerit2(Glib::ustring p_merit2);

		Glib::ustring GetMerit3();
		void SetMerit3(Glib::ustring p_merit3);

		Glib::ustring GetMerit4();
		void SetMerit4(Glib::ustring p_merit4);

		Glib::ustring GetMerit5();
		void SetMerit5(Glib::ustring p_merit5);

		Glib::ustring GetMerit6();
		void SetMerit6(Glib::ustring p_merit6);

		Glib::ustring GetMerit7();
		void SetMerit7(Glib::ustring p_merit7);
        
        // Merit Type
		Glib::ustring GetMeritType1();
		void SetMeritType1(Glib::ustring p_merittype1);

		Glib::ustring GetMeritType2();
		void SetMeritType2(Glib::ustring p_merittype2);

		Glib::ustring GetMeritType3();
		void SetMeritType3(Glib::ustring p_merittype3);

		Glib::ustring GetMeritType4();
		void SetMeritType4(Glib::ustring p_merittype4);

		Glib::ustring GetMeritType5();
		void SetMeritType5(Glib::ustring p_merittype5);

		Glib::ustring GetMeritType6();
		void SetMeritType6(Glib::ustring p_merittype6);

		Glib::ustring GetMeritType7();
		void SetMeritType7(Glib::ustring p_merittype7);

        // Merit Cost
		Glib::ustring GetMeritCost1();
		void SetMeritCost1(Glib::ustring p_meritCost1);

		Glib::ustring GetMeritCost2();
		void SetMeritCost2(Glib::ustring p_meritCost2);

		Glib::ustring GetMeritCost3();
		void SetMeritCost3(Glib::ustring p_meritCost3);

		Glib::ustring GetMeritCost4();
		void SetMeritCost4(Glib::ustring p_meritCost4);

		Glib::ustring GetMeritCost5();
		void SetMeritCost5(Glib::ustring p_meritCost5);

		Glib::ustring GetMeritCost6();
		void SetMeritCost6(Glib::ustring p_meritCost6);

		Glib::ustring GetMeritCost7();
		void SetMeritCost7(Glib::ustring p_meritCost7);
        
        // Flaw
        Glib::ustring GetFlaw1();
		void SetFlaw1(Glib::ustring p_flaw1);
		
        Glib::ustring GetFlaw2();
		void SetFlaw2(Glib::ustring p_flaw2);
		
        Glib::ustring GetFlaw3();
		void SetFlaw3(Glib::ustring p_flaw3);
		
        Glib::ustring GetFlaw4();
		void SetFlaw4(Glib::ustring p_flaw4);
		
        Glib::ustring GetFlaw5();
		void SetFlaw5(Glib::ustring p_flaw5);
		
        Glib::ustring GetFlaw6();
		void SetFlaw6(Glib::ustring p_flaw6);
		
        Glib::ustring GetFlaw7();
		void SetFlaw7(Glib::ustring p_flaw7);

        // Flaw type
		Glib::ustring GetFlawType1();
		void SetFlawType1(Glib::ustring p_flawType1);

		Glib::ustring GetFlawType2();
		void SetFlawType2(Glib::ustring p_flawType2);

		Glib::ustring GetFlawType3();
		void SetFlawType3(Glib::ustring p_flawType3);

		Glib::ustring GetFlawType4();
		void SetFlawType4(Glib::ustring p_flawType4);

		Glib::ustring GetFlawType5();
		void SetFlawType5(Glib::ustring p_flawType5);

		Glib::ustring GetFlawType6();
		void SetFlawType6(Glib::ustring p_flawType6);

		Glib::ustring GetFlawType7();
		void SetFlawType7(Glib::ustring p_flawType7);
        
        // Bonus
        Glib::ustring GetFlawBonus1();
		void SetFlawBonus1(Glib::ustring flawBonus1);

		Glib::ustring GetFlawBonus2();
		void SetFlawBonus2(Glib::ustring flawBonus2);

		Glib::ustring GetFlawBonus3();
		void SetFlawBonus3(Glib::ustring flawBonus3);

		Glib::ustring GetFlawBonus4();
		void SetFlawBonus4(Glib::ustring flawBonus4);

		Glib::ustring GetFlawBonus5();
		void SetFlawBonus5(Glib::ustring flawBonus5);

		Glib::ustring GetFlawBonus6();
		void SetFlawBonus6(Glib::ustring flawBonus6);

		Glib::ustring GetFlawBonus7();
		void SetFlawBonus7(Glib::ustring flawBonus7);


/* ********************************** */
        //PARTIE Other Traits
        //Name 1
        Glib::ustring GetOtherTraits1Name();
        void SetOtherTraits1Name(Glib::ustring p_otherTraits1Name);
        //Name 2
        Glib::ustring GetOtherTraits2Name();
        void SetOtherTraits2Name(Glib::ustring p_otherTraits2Name);
        //Name 3
        Glib::ustring GetOtherTraits3Name();
        void SetOtherTraits3Name(Glib::ustring p_otherTraits3Name);

        //Valeur 1
        Glib::ustring GetOtherTraits1Value();
        void SetOtherTraits1Value(Glib::ustring p_otherTraits1Value);
        //Valeur 2
        Glib::ustring GetOtherTraits2Value();
        void SetOtherTraits2Value(Glib::ustring p_otherTraits2Value);
        // Valeur 3
        Glib::ustring GetOtherTraits3Value();
        void SetOtherTraits3Value(Glib::ustring p_otherTraits3Value);

/* ********************************** */
        //Ritual and path
        //Rituals Name
        Glib::ustring GetRitualsName();
		void SetRitualsName(Glib::ustring p_ritualsName);
        //Rituals Level
        Glib::ustring GetRitualsLevel();
		void SetRitualsLevel(Glib::ustring p_ritualsLevel);

        //Path Name
        Glib::ustring GetPathsName();
		void SetPathsName(Glib::ustring p_pathsName);
        //Path Level
        Glib::ustring GetPathsLevel();
		void SetPathsLevel(Glib::ustring p_pathsLevel);

/* ********************************** */
        //PARTIE Experience  Derangements
        //total
        Glib::ustring GetTotal();
        void SetTotal(Glib::ustring p_total);
        //total Spent
        Glib::ustring GetTotalSpent();
        void SetTotalSpent(Glib::ustring p_totalSpent);
        //Spent On
        Glib::ustring GetSpentOn();
        void SetSpentOn(Glib::ustring p_spentOn);
        //derangements
        Glib::ustring GetDerangements();
        void SetDerangements(Glib::ustring p_derangements);

/* ********************************** */
        //PARTIE Combat
        // Tableau  Combat
        Glib::ustring GetcombatWeaponAttack();
        void SetcombatWeaponAttack(Glib::ustring p_combatWeaponAttack);

        Glib::ustring GetcombatDiff();
        void SetcombatDiff(Glib::ustring p_combatDiff);

        Glib::ustring GetcombatDamage();
        void SetcombatDamage(Glib::ustring p_combatDamage);

        Glib::ustring GetcombatRangee();
        void SetcombatRangee(Glib::ustring p_combatRange);

        Glib::ustring GetcombatRate();
        void SetcombatRate(Glib::ustring p_combatRate);

        Glib::ustring GetcombatClip();
        void SetcombatClip(Glib::ustring p_combatClip);

        Glib::ustring GetcombatConceal();
        void SetcombatConceal(Glib::ustring p_total);

        //Armor
        Glib::ustring GetarmorClass();
        void SetarmorClass(Glib::ustring p_armorClass);

        Glib::ustring GetarmorRating();
        void SetarmorRating(Glib::ustring p_armorRating);

        Glib::ustring GetarmorPenalty();
        void SetarmorPenalty(Glib::ustring p_armorPenalty);

        Glib::ustring GetarmorDescription();
        void SetarmorDescription(Glib::ustring p_armorDescription);

/* ********************************** */
        //PARTIE Expanded Backgrounds
        Glib::ustring Getallies();
        void Setallies(Glib::ustring p_allies);

        Glib::ustring Getcontacts();
        void Setcontacts(Glib::ustring p_contacts);

        Glib::ustring Getfame();
        void Setfame(Glib::ustring p_combatRange);

        Glib::ustring Getherd();
        void Setherd(Glib::ustring p_herd);

        Glib::ustring Getinfluence();
        void Setinfluence(Glib::ustring p_influence);

        Glib::ustring Getmentor();
        void Setmentor(Glib::ustring p_mentor);

        Glib::ustring Getresources();
        void Setresources(Glib::ustring Getresources);

        Glib::ustring Getretainers();
        void Setretainers(Glib::ustring p_retainers);

        Glib::ustring Getstatus();
        void Setstatus(Glib::ustring p_status);

        Glib::ustring Getother1();
        void Setother1(Glib::ustring p_other1);

        Glib::ustring Getother2();
        void Setother2(Glib::ustring p_other2);

/* ********************************** */
        //Possessions
        Glib::ustring Getgear();
        void Setgear(Glib::ustring p_gear);

        Glib::ustring GetfeedingGrounds();
        void SetfeedingGrounds(Glib::ustring p_feedingGrounds);

        Glib::ustring Getequipment();
        void Setequipment(Glib::ustring p_equipment);

        Glib::ustring Getvehicles();
        void Setvehicles(Glib::ustring p_vehicles);

/* ********************************** */
        //Blood   Bonds/Vinculi
        Glib::ustring GetboundToCol1();
        void SetboundToCol1(Glib::ustring p_boundToCol1);

        Glib::ustring GetboundToCol2();
        void SetboundToCol2(Glib::ustring p_boundToCol2);

        Glib::ustring Getratingcol1();
        void Setratingcol1(Glib::ustring p_ratingcol1);

        Glib::ustring Getratingcol2();
        void Setratingcol2(Glib::ustring p_ratingcol2);

/* ********************************** */
        //Havens
        Glib::ustring Getlocation();
        void Setlocation(Glib::ustring p_location);

        Glib::ustring GetdescriptionHavens();
        void SetdescriptionHavens(Glib::ustring p_descriptionHavens);

        /* ********************************** */
        //History
        Glib::ustring Getprelude();
        void Setprelude(Glib::ustring p_prelude);

        Glib::ustring Getgoals();
        void Setgoals(Glib::ustring p_goals);

/* ********************************** */
        //DESCRIPTION description
		Glib::ustring GetdescriptionAge();
		void SetdescriptionAge(Glib::ustring p_descriptionAge);

        Glib::ustring GetdescriptionApparentAge();
		void SetdescriptionApparentAge(Glib::ustring p_descriptionApparentAge);

        Glib::ustring GetdescriptionDateofBirth();
		void SetdescriptionDateofBirth(Glib::ustring p_descriptionDateofBirth);

        Glib::ustring GetdescriptionRIP();
		void SetdescriptionRIP(Glib::ustring p_descriptionRIP);

        Glib::ustring GetdescriptionHair();
		void SetdescriptionHair(Glib::ustring p_descriptionHair);

        Glib::ustring GetdescriptionEyes();
		void SetdescriptionEyes(Glib::ustring p_descriptionEyesLabel);

        Glib::ustring GetdescriptionRace();
		void SetdescriptionRace(Glib::ustring p_descriptionRace);

        Glib::ustring GetdescriptionNationality();
		void SetdescriptionNationality(Glib::ustring p_descriptionNationality);

        Glib::ustring GetdescriptionHeight();
		void SetdescriptionHeight(Glib::ustring p_descriptionHeight);

        Glib::ustring GetdescriptionWeight();
		void SetdescriptionWeight(Glib::ustring p_descriptionWeight);

        Glib::ustring GetdescriptionSex();
		void SetdescriptionSex(Glib::ustring p_descriptionSex);

        //DESCRIPTION description Col2
		Glib::ustring GetdescriptionAgeCol2();
		void SetdescriptionAgeCol2(Glib::ustring p_descriptionAgeCol2);

        Glib::ustring GetdescriptionApparentAgeCol2();
		void SetdescriptionApparentAgeCol2(Glib::ustring p_descriptionApparentAgeCol2);

        Glib::ustring GetdescriptionDateofBirthCol2();
		void SetdescriptionDateofBirthCol2(Glib::ustring p_descriptionDateofBirthCol2);

        Glib::ustring GetdescriptionRIPCol2();
		void SetdescriptionRIPCol2(Glib::ustring p_descriptionRIPCol2);

        Glib::ustring GetdescriptionHairCol2();
		void SetdescriptionHairCol2(Glib::ustring p_empathyAbilities);


        Glib::ustring GetdescriptionEyesCol2();
		void SetdescriptionEyesCol2(Glib::ustring p_descriptionEyesCol2);

        Glib::ustring GetdescriptionRaceCol2();
		void SetdescriptionRaceCol2(Glib::ustring p_descriptionRaceCol2);

        Glib::ustring GetdescriptionNationalityCol2();
		void SetdescriptionNationalityCol2(Glib::ustring p_descriptionHeightCol2);

        Glib::ustring GetdescriptionWeightCol2();
		void SetdescriptionWeightCol2(Glib::ustring p_descriptionWeightCol2);

        Glib::ustring GetdescriptionHeightCol2();
		void SetdescriptionHeightCol2(Glib::ustring p_descriptionHeightCol2);

        Glib::ustring GetdescriptionSexCol2();
		void SetdescriptionSexCol2(Glib::ustring p_descriptionSexCol2);




};

#endif // FENETRE_H_INCLUDED
