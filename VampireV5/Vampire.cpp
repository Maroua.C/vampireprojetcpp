#include "Vampire.h"
#include <vector>
#include <iostream> //entree sortie d'un fichier 
#include <fstream> // ecriture d'un flux dans un fichier 
#include <string>
#include <sstream> //gestion des chaine de caractere (dont caractere spéciaux)


//contient la partie class Clan et autre non utiliser pour l'affichage mais le code fonctionne -> utile pour une phrase d'amélioration du code
#include "Vampire01.pcpp"

//contient les get et set de la class TableCategories
#include "Vampire02.pcpp"


/* ***************************************************************** */
// Class qui contient les tableau de Clan, nature ...
TableCategories ChargerTableauxClanNatureetc()
{
    TableCategories tableCategories ;
    vector<string> listClan ; // Tableau des nom de clan
    vector<string> listNature ; // Tableau des nom de Nature

    // partie ATTRIBUT
    //physique
     vector<string> listStrength ; // tableau des Stength des Attributs
     vector<string> listDexterity ; // tableau des Dexterity des Attributs
     vector<string> listStamina; // tableau des Samina des Attributs

    //Social
     vector<string> listCharisma; // tableau des Charisma des Attributs
     vector<string> listManupilation; // tableau des Manipulation des Attributs
     vector<string> listAppearance; // tableau des Appearance des Attributs

    //Mental   
     vector<string> listPerception; // tableau des Perception des Attributs
     vector<string> listIntelligence; // tableau des Intelligence des Attributs
     vector<string> listWits; // tableau des Wits des Attributs

  
     vector<string> listDiscipline; // tableau des Discipline
     vector<string> listBackground; // tableau des Backgrounds

     vector<string> listMerits; // tableau des Merits
     vector<string> listMeritsType; // tableau des MeritsType
     vector<string> listFlaw; // tableau des MeritsType
     vector<string> listCost; // tableau des MeritsType

     vector<string> listPathName;  // tableau des Path
     vector<string> listRitualsName; // tableau des Rituals
     
     vector<string> listBackgrounds; //Backgrounds pour Other pour Expanded Backgrounds




     //gestion et ouverture d'un fichier en lecture ici la notice du jeu
    ifstream fichier("VampireV5.txt");
     string nomDuClan="", categorie="" ;
    //gestion de lecture et de parcourt du fichier
    if(fichier)
    {
      string ligne,attribut="",abilities="" ;
      int positionDebut = 0 ;

       while (getline(fichier, ligne))

        {   if (!ligne.empty())
            {
                //la ligne va tomber sur un des if qui correspond a une catégories dans le fichier et a chaque fois on implemente la variable caregorie

                // pour l'instant on ne trait que la partie Clans (##################################################################...)
                if ( ligne.substr(0, 10) == "#### Clans")
                {
                categorie = "Clans" ;
                }
                //ici on va selectonner la partie Nature
                if ( ligne.substr(0, 11) == "#### Nature")
                {
                    categorie = "Nature" ;
                }

                // la categorie attribut
                if ( ligne.substr(0, 15) == "#### Attributes")
                {
                    categorie = "Attributes" ;
                }

                //la categorie abilites
                if ( ligne.substr(0, 14) == "#### Abilities")
                {
                    categorie = "Abilities" ;
                }

                 //ici on va selectonner la partie Dicplines
                if ( ligne.substr(0, 16) == "#### Disciplines")
                {
                    categorie = "Disciplines" ;
                }
                 //ici on va selectonner la partie Backgrounds
                if ( ligne.substr(0, 16) == "#### Backgrounds")
                {
                    categorie = "Backgrounds" ;
                }

                 //ici on va selectonner la partie Virtues
                if ( ligne.substr(0, 12) == "#### Virtues")
                {
                    categorie = "Virtues" ;
                }

                 //ici on va selectonner la partie Merits and flaws"
                if ( ligne.substr(0, 21) == "#### Merits and flaws")
                {
                     categorie = "Merits and flaws" ;
                }

                //la categorie Rituals and Path
                //Path
                if ( ligne.substr(0, 26) == "#### Disciplines complexes")
                {
                    categorie = "Path" ;
                }
                //Rituals
                if ( ligne.substr(0, 24) == "#### Disciplines Rituals")
                {
                    categorie = "Rituals" ;
                }

                //la categorie Backgrounds pour Expanded Backgrounds
                if ( ligne.substr(0, 16) == "#### Backgrounds")
                {
                    categorie = "Backgrounds" ;
                }

/* ********************************** */
/* ********************************** */

                //teste la variable categorie implementer precedement
                if (categorie == "Clans")
                {
                    //si la variable caterogie vaut clan alors il va falloir extraire les noms de chaque clan
                   if (ligne.substr(0,2) == "--")
                   {
                      // On recpure le nom du Clan , le mot juste apres "--"
                      nomDuClan = ligne.substr(2) ;
                      listClan.push_back(nomDuClan) ;  // Charger nom Clan dans  listClan
                   }
                }
                /* ********************************** */

                if (categorie == "Nature")
                {
                    //si la variable caterogie vaut nature alors il va falloir extraire les noms de chaque nature
                   if (ligne.substr(0,2) == "--")
                   {
                      // On recpurer la nom de nature , le mot juste apres "--"
                      listNature.push_back(ligne.substr(2) ) ;  // Charger nom nature dans  listNature
                   }
                }
                /* ********************************** */

                if (categorie == "Attributes")
                {
                    //si la variable caterogie vaut Attributes alors il va falloir chercher les categorie d'attribut et les mettre tous dans un tableau different
                   if (ligne.substr(0,2) == "--")
                   {
                       attribut = ligne.substr(2) ;
                   }
                   if (attribut == "Strength")
                   {    positionDebut = ligne.substr(0, 5).find(":") ;
                        if (positionDebut > 0)
                        {
                           listStrength.push_back(ligne.substr(1, 100));
                        }
                   }
                   if (attribut == "Dexterity")
                   {    positionDebut = ligne.substr(0, 5).find(":") ;
                        if (positionDebut > 0)
                        {
                           listDexterity.push_back(ligne.substr(1, 100));
                        }
                   }
                   if (attribut == "Stamina")
                   {    positionDebut = ligne.substr(0, 5).find(":") ;
                        if (positionDebut > 0)
                        {
                           listStamina.push_back(ligne.substr(1, 100));
                        }
                   }
                    if (attribut == "Charisma")
                   {    positionDebut = ligne.substr(0, 5).find(":") ;
                        if (positionDebut > 0)
                        {
                           listCharisma.push_back(ligne.substr(1, 100));
                        }
                   }
                   if (attribut == "Manipulation")
                   {    positionDebut = ligne.substr(0, 5).find(":") ;
                        if (positionDebut > 0)
                        {
                           listManupilation.push_back(ligne.substr(1, 100));
                        }
                   }
                   if (attribut == "Appearance")
                   {    positionDebut = ligne.substr(0, 5).find(":") ;
                        if (positionDebut > 0)
                        {
                           listAppearance.push_back(ligne.substr(1, 100));
                        }
                   }
                   if (attribut == "Perception")
                   {
                      positionDebut = ligne.substr(0, 5).find(":") ;
                        if (positionDebut > 0)
                        {
                           listPerception.push_back(ligne.substr(1, 100));
                        }
                   }
                   if (attribut == "Intelligence")
                   {
                      positionDebut = ligne.substr(0, 5).find(":") ;
                        if (positionDebut > 0)
                        {
                           listIntelligence.push_back(ligne.substr(1, 100));
                        }
                   }
                   if (attribut == "Wits")
                   {
                      positionDebut = ligne.substr(0, 5).find(":") ;
                        if (positionDebut > 0)
                        {
                           listWits.push_back(ligne.substr(1, 100));
                        }
                   }

                }
                /* ********************************** */
                //ADVANTAGE
                if (categorie == "Disciplines")
                {
                   if (ligne.substr(0,2) == "--")
                   {
                      // On récpérer la nom du Clan , le mot juste aprés "--"
                      listDiscipline.push_back(ligne.substr(2) ) ;  
                   }
                }
                if (categorie == "Backgrounds")
                {
                   if (ligne.substr(0,2) == "--")
                   {
                      // On récpérer la nom du Clan , le mot juste aprés "--"
                      listBackground.push_back(ligne.substr(2) ) ;  
                   }
                }

                /* ********************************** */
                //Merit and Flaws
                if (categorie == "Merits and flaws" )
                {
                    if (ligne.substr(0,2) == "--")
                    {
                        positionDebut = ligne.find("Merit") ;
                        if (positionDebut > 0)
                        {
                           positionDebut = ligne.find("(") ;
                           listMerits.push_back(ligne.substr(2, positionDebut-2)) ;
                        }
                        positionDebut = ligne.find("Flaw") ;
                        if (positionDebut > 0)
                        {
                           positionDebut = ligne.find("(") ;
                           if (positionDebut >0) {listFlaw.push_back(ligne.substr(2, positionDebut-2)) ;}
                        }
                    }
                    if (ligne.substr(0,4) != "####" && ligne.substr(0,2)=="##" )
                    {
                        listMeritsType.push_back(ligne.substr(3)) ;
                    }
                }
                /* ********************************** */
                //la categorie Rituals and Path
                //Path
                if (categorie == "Path")
                {
                    if (ligne.substr(0,2) == "--")
                   {
                        // On recpurer la nom de Path , le mot juste apres "--"
                        listPathName.push_back(ligne.substr(2) ) ;

                   }
                }
                //Rituals
                if (categorie == "Rituals")
                {
                    if (ligne.substr(0,2) == "--")
                   {
                        // On recpurer la nom de Rituals , le mot juste apres "--"
                        listRitualsName.push_back(ligne.substr(2) ) ;

                   }
                }

                /* ********************************** */
                //PARTIE backgrounds
                if(categorie == "Backgrounds")
                {
                   if (ligne.substr(0,2) == "--")
                   {
                        // On recpurer la nom de backgrounds , le mot juste apres "--"
                        listBackgrounds.push_back(ligne.substr(2) ) ;

                   }
                }

            }
        }
    }
    else
    {
       cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
    }

    //le tableaux utiliser pour stocker les données du fichiers qu'on vient de parcourir (ou pas si on est dirrectmeent tomber dans le else )
    //tout les set qui n'ont pas de parametre ne lire pas le fichier VampireV5.txt
    //pour chaque categorie il y a un tableau adéquate de type vecteur (remarque pour chaque sous categorie aussi exemple attribut est une categorie , strenght(force) est une de ses sous categories)
    tableCategories.SetchargerNomClan(listClan);
    tableCategories.SetchargerNature(listNature);
    //PARTIE ATTRIBUT
    //physique
    tableCategories.SettableauStrength(listStrength);
    tableCategories.SettableauDexterity(listDexterity);
    tableCategories.SetTableauStamina(listStamina);

    //Social
    tableCategories.SetTableauCharisma(listCharisma);
    tableCategories.SetTableauManipulation(listManupilation);
    tableCategories.SetTableauAppearance(listAppearance);

    //Mental
    tableCategories.SetTableauPerception(listPerception) ;
    tableCategories.SetTableauIntelligence(listIntelligence) ;
    tableCategories.SetTableauWits(listWits) ;

    //Advantage
    tableCategories.SetTableauDiscipline(listDiscipline) ;
    tableCategories.SetTableauBackground(listBackground) ;

    //partie apres Advantage
    tableCategories.SetTableauHealth();
    tableCategories.SetTableauOtherTraits();

    //Merits and Flaws
    tableCategories.SetTableauMerit(listMerits);
    tableCategories.SetTableauMeritType(listMeritsType) ;
    tableCategories.SetTableauFlaw(listFlaw);


    //Partie Rituals Paths
    tableCategories.SetTableauRitualsPathLevel();
    //Lecture du fichier VampiveV5.txt
    tableCategories.SetTableauRitualsName(listRitualsName);
    tableCategories.SetTableauPathName(listPathName);

    //Partie Expanded Backgrounds
    //Charge le tableau Backgrounds pour la liste defileante
    tableCategories.SetTableauOtherExpandedBackgrounds(listBackgrounds);


    //Blood   Bonds/Vinculi
    tableCategories.SetTableauRating();

    //on retourne un tableua unique et on parcours le fichier qu'une seule fois avec extractions de toute les informatios utile
    return tableCategories ;
}


