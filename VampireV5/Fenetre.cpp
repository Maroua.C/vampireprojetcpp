// on appele pas tout la librairie mais uniquement les parties qui nous interesse cela évite de charger la librairie en entier
#include <gtkmm/button.h>   
#include <gtkmm/stock.h>
#include <gtkmm/alignment.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/image.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/scrolledwindow.h>

#include <gtkmm/main.h>

//librairie necessaire
#include <iostream> //Entree sortie d'un fichier 

//gestion des fichiers dans le programme, Ecriture d'un flux dans un fichier  
#include <fstream> 



//Le fichier Fenetre.cpp a ete decomposer en plusieurs sous fichier pour limiter le nombre de lignes dans le fichier.
#include "Fenetre01.pcpp"

// Set et Get de MERIT AND FLAWS
#include "Fenetre02.pcpp"
// Set et Get de AVANTAGE
#include "Fenetre03.pcpp" 
// Set et Get de  Partie Health (en dessous de Advantages)  Other trait ,Ritual and path et  Experience  Derangements 
#include "Fenetre04.pcpp"
// Set et Get de  Expanded Backgrounds,  Possesion,Blood Bonds/Vinculi,  Heaven,  history,  Description 
#include "Fenetre05.pcpp"


#include "Fenetre06.pcpp"

//contient la partie header donc les prototypes des fonctions
#include "Fenetre.h"
//La suite de .h donc les protytype
#include "Fenetre01.h"


#include "Vampire.h"

using namespace std;


/* ********************************************************************************* */
//constructeur de la classe fenetre

Fenetre::Fenetre() // : boutonValider("Agrandir Fenetre")
                    // :  boutonValider(Gtk::Stock::CANCEL)
{

/* ********************************** */
/// Partie pour amélioration du code cf ReadMe
/*
     vector<string> tabClan = chargerNomClan () ; // Charger les nom des Clan dans le tableau tabClan
     Charger les nom des Clan dans le tableau

    // Alimenter les caracteristique des clans dans tableau de clan
    vector<Clan> clanTab = AlimenterClassClan() ;
*/


    //TableCategories est une class dans les fichier Vampire (Vampire.cpp et Vampire.h)
    // on appele la class TableCategories via l'objet tableCategories et la methode qui permet de remplir un vecteur des clan, nature ou autre
    TableCategories tableCategories = ChargerTableauxClanNatureetc()  ;
    //Declare des tableaux de type vecteur et on appele la methode issue de tableCategories

    //Appelle des methodes spécifique de tableCategories pour chaque tableau de vecteur
    //charge le nom des clan dans tabClan
    vector<string> tabClan = tableCategories.GetchargerNomClan() ;
    //charge la nature  dans tabNature
    vector<string> tabNature = tableCategories.GetchargerNature() ;


/* ********************************** */
/* ********************************** */

//Les tableau sous forme de vecteur permettant de stocker les donnée issu des get issu de la class tableCategories du fichier Vampire.cpp
//Ces données sont dirrectement issue de fichier VampireV5.txt

    ///partie ATRIBUT
/* ********************************** */
    ///PHYSIQUE
    //charge la force (Strenght)  dans tabStrength
    vector<string> tabStrength = tableCategories.GettableauStrength() ;
    //charge la dexterité (Dexterity)  dans tabDexterity
    vector<string> tabDexterity = tableCategories.GetTableauDexterity() ;
    //charge l'endurance (Stamina)  dans tabStamina
    vector<string> tabStamina = tableCategories.GetTableauStamina() ;

    ///SOCIAL
    //charge Charisma  dans tabCharisma
    vector<string> tabCharisma = tableCategories.GetTableauCharisma() ; 
    //charge la Manipulation dans tabManipulation
    vector<string> tabManipulation= tableCategories.GetTableauManipulation() ; 
    //charge l'Appearance dans tabAppearance
    vector<string> tabAppearance = tableCategories.GetTableauAppearance() ; 

    ///MENTAL
    //charge Perception  dans tabPerception
    vector<string> tabPerception = tableCategories.GetTableauPerception() ; 
    //charge Intelligence  dans tabIntelligence
    vector<string> tabIntelligence = tableCategories.GetTableauIntelligence() ; 
    //charge la Wits   dans tabWits
    vector<string> tabWits = tableCategories.GetTableauWits() ; 

/* ********************************** */
    ///AVANTAGE
    //DISCIPLINE
   vector<string> tabDiscipline = tableCategories.GetTableauDiscipline() ; 
   //BACKGROUND
    vector<string> tabBackground = tableCategories.GetTableauBackground() ; 

/* ********************************** */
    ///MERITS AND FLAWS
    vector<string> tabMerits = tableCategories.GetTableauMerit() ; 
    vector<string> tabMeritsType = tableCategories.GetTableauMeritType() ; 
    vector<string> tabFlow = tableCategories.GetTableauFlaw() ; 

/* ********************************** */
    ///partie HEALTH
    ///Health obliger de declarer des variable differente a chaque fois car info qu'on a stocket dans le ficher n'est jamais la meme
    //on prend le meme GetTableauHealth car le contenu de chaque tableau est le meme
    vector<string> tabHealthBruised = tableCategories.GetTableauHealth() ;
    vector<string> tabHealthHurt = tableCategories.GetTableauHealth() ;
    vector<string> tabHealthInjured = tableCategories.GetTableauHealth() ;
    vector<string> tabHealthWounded = tableCategories.GetTableauHealth() ;
    vector<string> tabHealthMauled = tableCategories.GetTableauHealth() ;
    vector<string> tabHealthCrippled = tableCategories.GetTableauHealth() ;
    vector<string> tabHealthIncapacitated = tableCategories.GetTableauHealth() ;

/* ********************************** */
    ///partie Rituals  Paths
    //Level
    vector<string> tabRitualsLevel = tableCategories.GetTableauRitualsLevel() ;
    vector<string> tabPathLevel = tableCategories.GetTableauPathLevel() ;

    //Name
    vector<string> tabRitualsName = tableCategories.GetTableauRitualsName() ;
    vector<string> tabPathName = tableCategories.GetTableauPathName() ;

/* ********************************** */
    ///partie OTHER TRAIT
    vector<string> tabOtherTraits1 = tableCategories.GetTableauOtherTraits() ;
    vector<string> tabOtherTraits2 = tableCategories.GetTableauOtherTraits() ;
    vector<string> tabOtherTraits3 = tableCategories.GetTableauOtherTraits() ;

/* ********************************** */
    ///partie OTHER Expanded Background
    vector<string> tabOtherExpandedBackgrounds = tableCategories.GetTableauOtherExpandedBackgrounds() ;

/* ********************************** */
    ///partie Blood   Bonds/Vinculi
    vector<string> tabRating1 = tableCategories.GetTableauRating() ;
    vector<string> tabRating2 = tableCategories.GetTableauRating() ;



/* ********************************** */
/* ********************************** */
//Definit les widgets necessaire a l'application donc Label,Title,Entry ... 


    //tite de l'application
    set_title("Vampire" ) ;
    //taille de la bordure
    set_border_width(10) ;
    //taille par défaut minimal
    set_default_size(800, 800) ;


    //permet de definir la position de gtk ici au centre
    set_position(Gtk::WIN_POS_CENTER);

/* ********************************** */
    ///la partie titre
    
    // titre1 contient le text a afficher
    //obliger de divier en plusieur partie sinon probleme d'affichage
     titre11.set_markup("<b> <span font_family=\"Adobe Devanagari\" >   20th ANNIVERSARY EDITION  </span> </b>") ;
     titre12.set_markup("<b> <span font_family=\"Constantia\">   VAMPIRE   </span> </b>") ;
     titre13.set_markup("<b> <span font_family=\"Adobe Devanagari\" >  THE MASQUERADE   </span> </b> ") ;

     //inclu tout dans la boitre vertical, ce widget va contenir tout les autre widgets suivent (les autre boites)
     // Gtk::PACK_SHRINK : le widget ne prend que l'espace necessaire
     boite1.pack_start(titre11, Gtk::PACK_SHRINK) ;
     boite1.pack_start(titre12, Gtk::PACK_SHRINK) ;
     boite1.pack_start(titre13, Gtk::PACK_SHRINK) ;


/* ********************************** */
    ///La partie Nom
     // Le label et les paramtre de position du nom du joueur taper par l'utilisateur
     nameLabel.set_markup("<b> Name: </b>") ;
     nameEntry.set_alignment(Gtk::ALIGN_START) ;
     nameEntry.set_max_length(80) ;


/* ********************************** */
    /// Les autre labels necessaire (joueur,Demeaner, generation, chronide, concept, sire  )
     // Les parametres d'ajustements sont réaliser dans la boite3 (boite horizontale)
     playerLabel.set_markup("<b> Player: </b>") ;
     demeanerLabel.set_markup("<b> Demeaner: </b>") ;
     generationLabel.set_markup("<b> Generation: </b>") ;
     chronideLabel.set_markup("<b> Chronide: </b>") ;
     conceptLabel.set_markup("<b> Concept: </b>") ;
     sireLabel.set_markup("<b> Sire: </b>") ;


/* ********************************** */
     // Le label Clan
     listeDeroulanteClanLabel.set_markup("<b> Clan: </b>") ;
     // Charger le nom des clans dans la liste deroulante
     //la variable t prend la taille du tableau (le tableau est remplie on l'a remplie en amont)
     // le tableau tabClan est remplie des informations issue du fichier on a extrait les données d'interet
     int t = tabClan.size() ;
     //boucle for qui va crée le menu déroulant pour clan
     for (int i=0; i<tabClan.size(); i++)
     {
        // on va extraire element par element qu'on convertie en type Glib (pour l'afficher sur l'application)
        Glib::ustring nomClanustring = tabClan[i].c_str() ;
        //ainsi a chaque tour de boucle la variable listeDeroulanteClan sera implémenter
        // remarque : append_text ne marchais pas d'ou le append
        listeDeroulanteClan.append(nomClanustring) ;
     }


/* ********************************** */
     /// Le label nature meme principe qu'en haut pour le clan
     listeDeroulanteNatureLabel.set_markup("<b> Nature: </b>") ;
     // Charger le nom des nature  dans la liste deroulante
     for (int i=0; i<tabNature.size(); i++)
     {
        Glib::ustring nomNatureustring = tabNature[i].c_str() ;
        listeDeroulanteNature.append(nomNatureustring) ;
     }

/* ********************************** */
    ///Label permettant d'afficher la repartission de points dans l'application aide a l'utilisateur 
    pointsLabel.set_markup("<b> <span font_family=\"Constantia\"> Attributes: 7/5/3 • Abilities:13/9/5 • Disciplines:3 • Backgrounds:5 • Virtues:7</span></b> ");

/* ********************************** */
    ///ATTRIBUT

    //les labels pour les attributs les parametres d'ajustement de position seront fait dans le widget nomé boite4
     attributLabel.set_markup("-------------------------------------------------Attributs---------------------------------------------------------") ;

    /**********************/
    // physique
     attributPhysique.set_markup("<b> <span font_family=\"Constantia\"> Physique </span> </b>");
     strengthLabel.set_markup ("<b>Strength_____ </b>") ;
     dexterityLabel.set_markup("<b>Dexterity____ </b>") ;
     staminaLabel.set_markup  ("<b>Stamina______ </b>") ;

    /**********************/
    // Social
     attributSocial.set_markup("<b> <span font_family=\"Constantia\"> Social</span> </b>");
     charismaLabel.set_markup     ("<b>Charisma__________ </b>") ;
     manipulationLabel.set_markup ("<b>Manipulation______ </b>") ;
     apprearanceLabel.set_markup  ("<b>appearance________</b>") ;

    /**********************/
    // Mental
     attributMental.set_markup("<b> <span font_family=\"Constantia\"> Mental</span> </b>");
     perceptionLabel.set_markup  ("<b>Perception______</b>") ;
     intelligenceLabel.set_markup("<b>Intelligence____</b>") ;
     witsLabel.set_markup        ("<b>Wits____________</b>") ;



    // Charger les caracteristiue de l'attribut Strength meme principe que nature et clan
     for (int i=0; i<tabStrength.size(); i++)
     {
        Glib::ustring nomSrentghustring = tabStrength[i].c_str() ;
        strengthlisteDeroulante.append(nomSrentghustring) ;
     }
     // Charger les caracteristiue de l'attribut Dexterity meme principe que nature et clan
     for (int i=0; i<tabDexterity.size(); i++)
     {
        Glib::ustring nomDexterityustring = tabDexterity[i].c_str() ;
        dexteritylisteDeroulante.append(nomDexterityustring) ;
     }
     // Charger les caracteristiue de l'attribut Stamina meme principe que nature et clan
     for (int i=0; i<tabStamina.size(); i++)
     {
        Glib::ustring nomStaminaustring = tabStamina[i].c_str() ;
        staminalisteDeroulante.append(nomStaminaustring) ;
     }

    // Charger les caractéristiue de l'attribut Charisma
     charismalisteDeroulante.append("") ;
     for (int i=0; i<tabCharisma.size(); i++)
     {
        Glib::ustring nomCharismaustring = tabCharisma[i].c_str() ;
        charismalisteDeroulante.append(nomCharismaustring) ;
     }

     // Charger les caractéristiue de l'attribut Manipulation
     manipulationlisteDeroulante.append("") ;
     for (int i=0; i<tabManipulation.size(); i++)
     {
        Glib::ustring nomManipulationustring = tabManipulation[i].c_str() ;
        manipulationlisteDeroulante.append(nomManipulationustring) ;
     }

     // Charger les caractéristiue de l'attribut Manipulation
     apprearancelisteDeroulante.append("") ;
     for (int i=0; i<tabAppearance.size(); i++)
     {
        Glib::ustring nomAppearanceustring = tabAppearance[i].c_str() ;
        apprearancelisteDeroulante.append(nomAppearanceustring) ;
     }

     // Charger les caractéristiue de l'attribut Perception
     perceptionlisteDeroulante.append("") ;
     for (int i=0; i<tabPerception.size(); i++)
     {
        Glib::ustring nomPerceptionustring = tabPerception[i].c_str() ;
        perceptionlisteDeroulante.append(nomPerceptionustring) ;
     }

     // Charger les caractéristiue de l'attribut Intelligence
     intelligencelisteDeroulante.append("") ;
     for (int i=0; i<tabIntelligence.size(); i++)
     {
        Glib::ustring nomIntelligenceustring = tabIntelligence[i].c_str() ;
        intelligencelisteDeroulante.append(nomIntelligenceustring) ;
     }

     // Charger les caractéristiue de l'attribut Wits
     witslisteDeroulante.append("") ;
     for (int i=0; i<tabWits.size(); i++)
     {
        Glib::ustring nomWitsustring = tabWits[i].c_str() ;
        witslisteDeroulante.append(nomWitsustring) ;
     }



/* ********************************** */
///ABILITIES

    //les labels pour les abilities les parametres d'ajustement de position seront fait dans le widget nomé boite4
     abilitiesLabel.set_markup("-------------------------------------------------Abilities---------------------------------------------------------") ;

    /**********************/
    // Talents
     abilitiesTalents.set_markup("<b> <span font_family=\"Constantia\"> Talents </span> </b>");
     alertnessLabel.set_markup ("<b>Alertness____ </b>") ;
     awarenessLabel.set_markup  ("<b>Awareness_____</b>") ;
     athleticsLabel.set_markup ("<b>Athletics_____ </b>") ;
     brawlLabel.set_markup ("<b>Brawl_____  </b>") ;
     empathyLabel.set_markup ("<b>Empathy_____  </b>") ;
     expressionLabel.set_markup ("<b>Expression_____  </b>") ;
     intimidationLabel.set_markup ("<b>Intimidation_____  </b>") ;
     leadershipLabel.set_markup ("<b>Leadership_____  </b>") ;
     streetwiseLabel.set_markup ("<b>Streetwise_____  </b>") ;
     subterfugeLabel.set_markup ("<b>Subterfuge_____  </b>") ;
     hobbyTalentLabel.set_markup ("<b>Hobby Talent_____  </b>") ;


    /**********************/
    //skills
     abilitiesSkills.set_markup("<b> <span font_family=\"Skills\"> Skills</span> </b>");
     animalkenLabel.set_markup  ("<b>Animal Ken_____</b>") ;
     craftsLabel.set_markup ("<b>Crafts_____ </b>") ;
     driveLabel.set_markup  ("<b>Drive_____</b>") ;
     etiquetteLabel.set_markup  ("<b>Etiquette_____</b>") ;
     firearmsLabel.set_markup  ("<b>Firearms_____</b>") ;
     larcenyLabel.set_markup  ("<b>Larceny_____</b>") ;
     meleeLabel.set_markup  ("<b>Melee_____</b>") ;
     performanceLabel.set_markup  ("<b>Performance_____</b>") ;
     stealthLabel.set_markup  ("<b>Stealth_____</b>") ;
     survivalLabel.set_markup  ("<b>Survival_____</b>") ;
     professionalSkillLabel.set_markup  ("<b>Professional Skill_____</b>") ;



    /**********************/
    //Knowledges
     abilitiesKnowledges.set_markup("<b> <span font_family=\"Constantia\"> Knowledges</span> </b>");
     academicsLabel.set_markup  ("<b>Academics</b>") ;
     computerLabel.set_markup("<b>Computer____</b>") ;
     financeLabel.set_markup  ("<b>Finance___________</b>") ;
     investigationLabel.set_markup  ("<b>Investigation_________</b>") ;
     lawLabel.set_markup  ("<b>Law___________</b>") ;
     medicineLabel.set_markup  ("<b>Medicine___________</b>") ;
     occultLabel.set_markup  ("<b>Occult___________</b>") ;
     politicsLabel.set_markup  ("<b>Politics___________</b>") ;
     scienceLabel.set_markup  ("<b>Science___________</b>") ;
     technologyLabel.set_markup  ("<b>Technology___________</b>") ;
     expertLabel.set_markup  ("<b>Expert___________</b>") ;



/* ********************************** */
    /// AVANTAGE

     advantageLabel.set_markup("-------------------------------------------------Advantage---------------------------------------------------------") ;
     advantageDiscipline.set_markup("<b> <span font_family=\"Constantia\"> Discipline </span> </b>");
     advantageBackground.set_markup("<b> <span font_family=\"Constantia\"> Background </span> </b>");
     advantageVirtue.set_markup("<b> <span font_family=\"Constantia\"> Virtue </span> </b>");

     virtue1Label.set_markup  ("<b>Conscience/Conviction</b>") ;
     virtue2Label.set_markup  ("<b>Self-Control/Instinct</b>") ;
     virtue3Label.set_markup  ("<b>Courage</b>") ;


/* ********************************** */
    /// ADVANTAGE
    
     // Charger les caractéristiue des discpline une nouvelle boucle pour chaque zone de texte 

    discipline1listeDeroulante.append("") ;
     for (int i=0; i<tabDiscipline.size(); i++)
     {
        Glib::ustring nomDiscplineWitsustring = tabDiscipline[i].c_str() ;
        discipline1listeDeroulante.append(nomDiscplineWitsustring) ;
     }

     discipline2listeDeroulante.append("") ;
     for (int i=0; i<tabDiscipline.size(); i++)
     {
        Glib::ustring nomDiscplineWitsustring = tabDiscipline[i].c_str() ;
        discipline2listeDeroulante.append(nomDiscplineWitsustring) ;
     }

     discipline3listeDeroulante.append("") ;
     for (int i=0; i<tabDiscipline.size(); i++)
     {
        Glib::ustring nomDiscplineWitsustring = tabDiscipline[i].c_str() ;
        discipline3listeDeroulante.append(nomDiscplineWitsustring) ;
     }

     discipline4listeDeroulante.append("") ;
     for (int i=0; i<tabDiscipline.size(); i++)
     {
        Glib::ustring nomDiscplineWitsustring = tabDiscipline[i].c_str() ;
        discipline4listeDeroulante.append(nomDiscplineWitsustring) ;
     }


     discipline5listeDeroulante.append("") ;
     for (int i=0; i<tabDiscipline.size(); i++)
     {
        Glib::ustring nomDiscplineWitsustring = tabDiscipline[i].c_str() ;
        discipline5listeDeroulante.append(nomDiscplineWitsustring) ;
     }

     discipline6listeDeroulante.append("") ;
     for (int i=0; i<tabDiscipline.size(); i++)
     {
        Glib::ustring nomDiscplineWitsustring = tabDiscipline[i].c_str() ;
        discipline6listeDeroulante.append(nomDiscplineWitsustring) ;
     }

     backgroud1listeDeroulante.append("") ;
     backgroud2listeDeroulante.append("") ;
     backgroud3listeDeroulante.append("") ;
     backgroud4listeDeroulante.append("") ;
     backgroud5listeDeroulante.append("") ;
     backgroud6listeDeroulante.append("") ;

     for (int i=0; i<tabBackground.size(); i++)
     {
        Glib::ustring nomBackgroundWitsustring = tabBackground[i].c_str() ;
        backgroud1listeDeroulante.append(nomBackgroundWitsustring) ;
        backgroud2listeDeroulante.append(nomBackgroundWitsustring) ;
        backgroud3listeDeroulante.append(nomBackgroundWitsustring) ;
        backgroud4listeDeroulante.append(nomBackgroundWitsustring) ;
        backgroud5listeDeroulante.append(nomBackgroundWitsustring) ;
        backgroud6listeDeroulante.append(nomBackgroundWitsustring) ;
     }

/* ********************************** */
///PARTIE EN BAS DE ADVANTAGE


    transitionLabel.set_markup("----------------------------------------------------------------------------------------------------------") ;
    tapeHereLabel.set_markup  ("<b>Tape Here</b>") ;
    humanityPathLabel.set_markup  ("-----HumanityPath-----") ;
    willpowerLabel.set_markup  ("-----Willpower-----") ;
    bloodPoolLabel.set_markup  ("-----Blood Pool-----") ;

    healthPathLabel.set_markup  ("-----Health-----") ;
    bruisedLabel.set_markup  ("Bruised      ") ;
    hurtLabel.set_markup  ("Hurt      -1") ;
    injuredLabel.set_markup  ("Injured      -1") ;
    woundedLabel.set_markup  ("Wounded      -2") ;
    mauledLabel.set_markup  ("Mauled      -2") ;
    crippledLabel.set_markup  ("Crippled      -5") ;
    incapacitatedLabel.set_markup  ("Incapacitated      ") ;

    weaknessLabel.set_markup  ("-----Weakness-----") ;
    experienceLabel.set_markup  ("-----Experience-----") ;


     //HEALTH
     //obliger de declarer des variable differente a chaque fois car info qu'on a stocket dans le ficher n'est jamais la meme

      // Charger les caracteristiue de l'attribut Health Bruised meme principe que nature et clan
    for (int i=0; i<tabHealthBruised.size(); i++)
     {
        Glib::ustring nomHealthaustring = tabHealthBruised[i].c_str() ;
        healthlisteDeroulanteBruised.append(nomHealthaustring) ;
     }

      // Charger les caracteristiue de l'attribut HealthHurt meme principe que nature et clan
    for (int i=0; i<tabHealthHurt.size(); i++)
     {
        Glib::ustring nomHealthaustring = tabHealthHurt[i].c_str() ;
        healthlisteDeroulanteHurt.append(nomHealthaustring) ;
     }

      // Charger les caracteristiue de l'attribut HealthInjured meme principe que nature et clan
    for (int i=0; i<tabHealthInjured.size(); i++)
     {
        Glib::ustring nomHealthaustring = tabHealthInjured[i].c_str() ;
        healthlisteDeroulanteInjured.append(nomHealthaustring) ;
     }

      // Charger les caracteristiue de l'attribut HealthWounded meme principe que nature et clan
    for (int i=0; i<tabHealthWounded.size(); i++)
     {
        Glib::ustring nomHealthaustring = tabHealthWounded[i].c_str() ;
        healthlisteDeroulanteWounded.append(nomHealthaustring) ;
     }

      // Charger les caracteristiue de l'attribut HealthMauled meme principe que nature et clan
    for (int i=0; i<tabHealthMauled.size(); i++)
     {
        Glib::ustring nomHealthaustring = tabHealthMauled[i].c_str() ;
        healthlisteDeroulanteMauled.append(nomHealthaustring) ;
     }

      // Charger les caracteristiue de l'attribut HealthCrippled meme principe que nature et clan
    for (int i=0; i<tabHealthCrippled.size(); i++)
     {
        Glib::ustring nomHealthaustring = tabHealthCrippled[i].c_str() ;
        healthlisteDeroulanteCrippled.append(nomHealthaustring) ;
     }

      // Charger les caracteristiue de l'attribut HealthIncapacitated meme principe que nature et clan
    for (int i=0; i<tabHealthIncapacitated.size(); i++)
     {
        Glib::ustring nomHealthaustring = tabHealthIncapacitated[i].c_str() ;
        healthlisteDeroulanteIncapacitated.append(nomHealthaustring) ;
     }




/* ********************************** */
    /// MERITS AND FLAWS

     meritANDflawLabel.set_markup("---------------------------------------------------------------Merits AND Flaws----------------------------------") ;
     MeritLabel.set_markup("<b> <span font_family=\"Constantia\"> Merit </span> </b>");
     meritTypeLabel.set_markup("<b> <span font_family=\"Constantia\"> Type </span> </b>");
     CostMeritLabel.set_markup("<b> <span font_family=\"Constantia\"> Cost </span> </b>");
     FlawLabel.set_markup("<b> <span font_family=\"Constantia\"> Flaw </span> </b>");
     flawTypeLabel.set_markup("<b> <span font_family=\"Constantia\"> Type </span> </b>");
     BonusLabel.set_markup("<b> <span font_family=\"Constantia\"> Bonus </span> </b>");


/* ************* */

    // etant donné qu'on a besoin de liste déroulante alors incrementation de facon manuelle de ces listes pour ainsi pouvoir l'afficher dans l'applicaiton
    // 7 liste car il y a 7 ligne 
    
     merit1listeDeroulante.append("") ;
     merit2listeDeroulante.append("") ;
     merit3listeDeroulante.append("") ;
     merit4listeDeroulante.append("") ;
     merit5listeDeroulante.append("") ;
     merit6listeDeroulante.append("") ;
     merit7listeDeroulante.append("") ;

     // A chaque colonne on va incrementer le tableau necessaire 
     for (int i=0; i<tabMerits.size(); i++)
     {
        Glib::ustring nomMeritustring = tabMerits[i].c_str() ;
        merit1listeDeroulante.append(nomMeritustring) ;
        merit2listeDeroulante.append(nomMeritustring) ;
        merit3listeDeroulante.append(nomMeritustring) ;
        merit4listeDeroulante.append(nomMeritustring) ;
        merit5listeDeroulante.append(nomMeritustring) ;
        merit6listeDeroulante.append(nomMeritustring) ;
        merit7listeDeroulante.append(nomMeritustring) ;
     }


    // 2eme colonne le TYPE de MERITS qui contient 7 ligne aussi 
     meritType1listeDeroulante.append("") ;
     meritType2listeDeroulante.append("") ;
     meritType3listeDeroulante.append("") ;
     meritType4listeDeroulante.append("") ;
     meritType5listeDeroulante.append("") ;
     meritType6listeDeroulante.append("") ;
     meritType7listeDeroulante.append("") ;

    // A chaque colonne on va incrementer le tableau necessaire 
     for (int i=0; i<tabMeritsType.size(); i++)
     {
        Glib::ustring nomMeritTypeustring = tabMeritsType[i].c_str() ;
        meritType1listeDeroulante.append(nomMeritTypeustring) ;
        meritType2listeDeroulante.append(nomMeritTypeustring) ;
        meritType3listeDeroulante.append(nomMeritTypeustring) ;
        meritType4listeDeroulante.append(nomMeritTypeustring) ;
        meritType5listeDeroulante.append(nomMeritTypeustring) ;
        meritType6listeDeroulante.append(nomMeritTypeustring) ;
        meritType7listeDeroulante.append(nomMeritTypeustring) ;
     }

    // 4eme colonne  de FLAW qui contient 7 ligne aussi 
     flow1listeDeroulante.append("") ;
     flow2listeDeroulante.append("") ;
     flow3listeDeroulante.append("") ;
     flow4listeDeroulante.append("") ;
     flow5listeDeroulante.append("") ;
     flow6listeDeroulante.append("") ;
     flow7listeDeroulante.append("") ;

    // A chaque colonne on va incrementer le tableau necessaire 
     for (int i=0; i<tabFlow.size(); i++)
     {
        Glib::ustring nomFlawustring = tabFlow[i].c_str() ;
        flow1listeDeroulante.append(nomFlawustring) ;
        flow2listeDeroulante.append(nomFlawustring) ;
        flow3listeDeroulante.append(nomFlawustring) ;
        flow4listeDeroulante.append(nomFlawustring) ;
        flow5listeDeroulante.append(nomFlawustring) ;
        flow6listeDeroulante.append(nomFlawustring) ;
        flow7listeDeroulante.append(nomFlawustring) ;
     }

    // meme principe avec la collone cost (3eme colonne )
    //Contient 7 ligne d'ou les 7 listes
     meritCost1listeDeroulante.append("") ;
     meritCost1listeDeroulante.append("1") ;
     meritCost1listeDeroulante.append("2") ;
     meritCost1listeDeroulante.append("3") ;
     meritCost1listeDeroulante.append("4") ;
     meritCost1listeDeroulante.append("5") ;
     meritCost1listeDeroulante.append("6") ;
     meritCost1listeDeroulante.append("7") ;

     meritCost2listeDeroulante.append("") ;
     meritCost2listeDeroulante.append("1") ;
     meritCost2listeDeroulante.append("2") ;
     meritCost2listeDeroulante.append("3") ;
     meritCost2listeDeroulante.append("4") ;
     meritCost2listeDeroulante.append("5") ;
     meritCost2listeDeroulante.append("6") ;
     meritCost2listeDeroulante.append("7") ;

     meritCost3listeDeroulante.append("") ;
     meritCost3listeDeroulante.append("1") ;
     meritCost3listeDeroulante.append("2") ;
     meritCost3listeDeroulante.append("3") ;
     meritCost3listeDeroulante.append("4") ;
     meritCost3listeDeroulante.append("5") ;
     meritCost3listeDeroulante.append("6") ;
     meritCost3listeDeroulante.append("7") ;

     meritCost4listeDeroulante.append("") ;
     meritCost4listeDeroulante.append("1") ;
     meritCost4listeDeroulante.append("2") ;
     meritCost4listeDeroulante.append("3") ;
     meritCost4listeDeroulante.append("4") ;
     meritCost4listeDeroulante.append("5") ;
     meritCost4listeDeroulante.append("6") ;
     meritCost4listeDeroulante.append("7") ;

     meritCost5listeDeroulante.append("") ;
     meritCost5listeDeroulante.append("1") ;
     meritCost5listeDeroulante.append("2") ;
     meritCost5listeDeroulante.append("3") ;
     meritCost5listeDeroulante.append("4") ;
     meritCost5listeDeroulante.append("5") ;
     meritCost5listeDeroulante.append("6") ;
     meritCost5listeDeroulante.append("7") ;

     meritCost6listeDeroulante.append("") ;
     meritCost6listeDeroulante.append("1") ;
     meritCost6listeDeroulante.append("2") ;
     meritCost6listeDeroulante.append("3") ;
     meritCost6listeDeroulante.append("4") ;
     meritCost6listeDeroulante.append("5") ;
     meritCost6listeDeroulante.append("6") ;
     meritCost6listeDeroulante.append("7") ;

     meritCost7listeDeroulante.append("") ;
     meritCost7listeDeroulante.append("1") ;
     meritCost7listeDeroulante.append("2") ;
     meritCost7listeDeroulante.append("3") ;
     meritCost7listeDeroulante.append("4") ;
     meritCost7listeDeroulante.append("5") ;
     meritCost7listeDeroulante.append("6") ;
     meritCost7listeDeroulante.append("7") ;

    //incrementation de type de MERIT

     meritType1listeDeroulante.append("") ;
     meritType2listeDeroulante.append("") ;
     meritType3listeDeroulante.append("") ;
     meritType4listeDeroulante.append("") ;
     meritType5listeDeroulante.append("") ;
     meritType6listeDeroulante.append("") ;
     meritType7listeDeroulante.append("") ;

    //incrementation de type de FLAWS (nomme flows dans la variable de type list ci dessous)
     flowType1listeDeroulante.append("") ;
     flowType2listeDeroulante.append("") ;
     flowType3listeDeroulante.append("") ;
     flowType4listeDeroulante.append("") ;
     flowType5listeDeroulante.append("") ;
     flowType6listeDeroulante.append("") ;
     flowType7listeDeroulante.append("") ;
      // A chaque colonne on va incrementer le tableau necessaire 
     for (int i=0; i<tabMeritsType.size(); i++)
     {
        Glib::ustring nomMeritTypeustring = tabMeritsType[i].c_str() ;
        flowType1listeDeroulante.append(nomMeritTypeustring) ;
        flowType2listeDeroulante.append(nomMeritTypeustring) ;
        flowType3listeDeroulante.append(nomMeritTypeustring) ;
        flowType4listeDeroulante.append(nomMeritTypeustring) ;
        flowType5listeDeroulante.append(nomMeritTypeustring) ;
        flowType6listeDeroulante.append(nomMeritTypeustring) ;
        flowType7listeDeroulante.append(nomMeritTypeustring) ;
     }

     //meme principe pour les bonus derniere colonnes qui fait partie de FLAWS et contient 7 lignes égalements 
     bonus1listeDeroulante.append("") ;
     bonus1listeDeroulante.append("1") ;
     bonus1listeDeroulante.append("2") ;
     bonus1listeDeroulante.append("3") ;
     bonus1listeDeroulante.append("4") ;
     bonus1listeDeroulante.append("5") ;
     bonus1listeDeroulante.append("6") ;
     bonus1listeDeroulante.append("7") ;

     bonus2listeDeroulante.append("") ;
     bonus2listeDeroulante.append("1") ;
     bonus2listeDeroulante.append("2") ;
     bonus2listeDeroulante.append("3") ;
     bonus2listeDeroulante.append("4") ;
     bonus2listeDeroulante.append("5") ;
     bonus2listeDeroulante.append("6") ;
     bonus2listeDeroulante.append("7") ;

     bonus3listeDeroulante.append("") ;
     bonus3listeDeroulante.append("1") ;
     bonus3listeDeroulante.append("2") ;
     bonus3listeDeroulante.append("3") ;
     bonus3listeDeroulante.append("4") ;
     bonus3listeDeroulante.append("5") ;
     bonus3listeDeroulante.append("6") ;
     bonus3listeDeroulante.append("7") ;

     bonus4listeDeroulante.append("") ;
     bonus4listeDeroulante.append("1") ;
     bonus4listeDeroulante.append("2") ;
     bonus4listeDeroulante.append("3") ;
     bonus4listeDeroulante.append("4") ;
     bonus4listeDeroulante.append("5") ;
     bonus4listeDeroulante.append("6") ;
     bonus4listeDeroulante.append("7") ;

     bonus5listeDeroulante.append("") ;
     bonus5listeDeroulante.append("1") ;
     bonus5listeDeroulante.append("2") ;
     bonus5listeDeroulante.append("3") ;
     bonus5listeDeroulante.append("4") ;
     bonus5listeDeroulante.append("5") ;
     bonus5listeDeroulante.append("6") ;
     bonus5listeDeroulante.append("7") ;

     bonus6listeDeroulante.append("") ;
     bonus6listeDeroulante.append("1") ;
     bonus6listeDeroulante.append("2") ;
     bonus6listeDeroulante.append("3") ;
     bonus6listeDeroulante.append("4") ;
     bonus6listeDeroulante.append("5") ;
     bonus6listeDeroulante.append("6") ;
     bonus6listeDeroulante.append("7") ;

     bonus7listeDeroulante.append("") ;
     bonus7listeDeroulante.append("1") ;
     bonus7listeDeroulante.append("2") ;
     bonus7listeDeroulante.append("3") ;
     bonus7listeDeroulante.append("4") ;
     bonus7listeDeroulante.append("5") ;
     bonus7listeDeroulante.append("6") ;
     bonus7listeDeroulante.append("7") ;



/* ********************************** */
    //Other trait 1

    
    /* ********************************************************* */
    // partie  Other Traits
    otherTraitsLabel.set_markup("-------------------------------------------------Other Traits---------------------------------------------------------") ;

    // Experience Derangements
    experienceDerangementsLabel.set_markup("----------------------------------------------------------------------------------------------------------") ;
    experienceDLabel.set_markup  (" Experience") ;
    totalLabel.set_markup  ("Total:") ;
    totalSpentLabel.set_markup  ("Total Spent:") ;
    spentOnLabel.set_markup  ("Spent On:") ;
    derangementsLabel.set_markup  (" Derangements") ;


    for (int i=0; i<tabOtherTraits1.size(); i++)
     {
        Glib::ustring nomOTaustring = tabOtherTraits1[i].c_str() ;
        listeDeroulanteOtherTraits1.append(nomOTaustring) ;
     }

    //Other trait 2
    for (int i=0; i<tabOtherTraits2.size(); i++)
     {
        Glib::ustring nomOTaustring = tabOtherTraits2[i].c_str() ;
        listeDeroulanteOtherTraits2.append(nomOTaustring) ;
     }
     //Other trait 3
    for (int i=0; i<tabOtherTraits3.size(); i++)
     {
        Glib::ustring nomOTaustring = tabOtherTraits3[i].c_str() ;
        listeDeroulanteOtherTraits3.append(nomOTaustring) ;
     }


/* ********************************** */
    //RITUALS PATH
    //LEVEL
    //Rituals
    for (int i=0; i<tabRitualsLevel.size(); i++)
     {
        Glib::ustring ritualstring = tabRitualsLevel[i].c_str() ;
        listeDeroulanteritualsLevel.append(ritualstring) ;
     }

    //Paths
    for (int i=0; i<tabPathLevel.size(); i++)
     {
        Glib::ustring pathstring = tabPathLevel[i].c_str() ;
        listeDeroulantepathsLevel.append(pathstring) ;
     }

    //NAME
    //Rituals
    for (int i=0; i<tabRitualsName.size(); i++)
     {
        Glib::ustring ritualNamestring = tabRitualsName[i].c_str() ;
        listeDeroulanteritualsName.append(ritualNamestring) ;
     }

    //Paths
    for (int i=0; i<tabPathName.size(); i++)
     {
        Glib::ustring pathNamestring = tabPathName[i].c_str() ;
        listeDeroulantepathsName.append(pathNamestring) ;
     }



/* ********************************** */
    ///COMBAT
    // partie  Combat
    combatLabel.set_markup("-------------------------------------------------Combat---------------------------------------------------------") ;

    // Le pseudo tableau
    combatWeaponAttackLabel.set_markup("Weapon/Attack") ;
    combatDiffLabel.set_markup  (" Diff") ;
    combatDamageLabel.set_markup  (" Damage") ;
    combatRangeLabel.set_markup  ("Range:") ;
    combatRateLabel.set_markup  ("Rate") ;
    combatClipLabel.set_markup  ("Clip") ;
    combatConcealLabel.set_markup  (" Conceal") ;

    // armor partie
    armorArmorLabel.set_markup("----Armor----") ;
    armorClassLabel.set_markup  (" Class") ;
    armorRatingLabel.set_markup  ("Rating:") ;
    armorPenaltyLabel.set_markup  ("Penalty:") ;
    armorDescriptionLabel.set_markup  ("Description:") ;



/* ********************************** */
    ///Expanded Backgrounds

    expandedBackgroundsLabel.set_markup("-------------------------------------------------Expanded Backgrounds--------------------------------------------------------") ;
    alliesLabel.set_markup  ("Allies ") ;
    mentorLabel.set_markup  ("Mentor ") ;
    contactsLabel.set_markup  ("Contacts ") ;
    resourcesLabel.set_markup  ("Resources ") ;
    fameLabel.set_markup  ("Fame ") ;
    retainersLabel.set_markup  ("Retainers ") ;
    herdLabel.set_markup  ("Herd ") ;
    statusLabel.set_markup  ("Status ") ;
    influenceLabel.set_markup  ("Influence ") ;
    otherLabel.set_markup  ("Other ") ;



     for (int i=0; i<tabOtherExpandedBackgrounds.size(); i++)
     {
        Glib::ustring OtherExpandedBackgroundsstring = tabOtherExpandedBackgrounds[i].c_str() ;
        other1listeDeroulante.append(OtherExpandedBackgroundsstring) ;
     }

/* ********************************** */
    ///Possessions
    possessionsLabel.set_markup("-------------------------------------------------Possessions--------------------------------------------------------") ;
    gearLabel.set_markup  ("Gear(Carried)(Carried)ies ") ;
    feedingGroundsLabel.set_markup  ("Feeding Grounds ") ;

    equipmentLabel.set_markup  ("Equipment(Owned) ") ;
    vehiclesLabel.set_markup  ("Vehicles ") ;


/* ********************************** */
    ///Blood Bonds Vinculi

    bloodBondsVinculiLabel.set_markup("-------------------------------------------------Blood  Bonds/Vinculi--------------------------------------------------------") ;
    boundToLabelcol1.set_markup  ("Bound To ") ;
    ratingLabelcol1.set_markup  ("Rating") ;

    boundToLabelcol2.set_markup  ("Bound To  ") ;
    ratingLabelcol2.set_markup  ("Rating ") ;


     //Tab Rating
    for (int i=0; i<tabRating1.size(); i++)
     {
        Glib::ustring nomOTaustring = tabRating1[i].c_str() ;
        ratinglisteDeroulantecol1.append(nomOTaustring) ;
     }

     //Tab Rating
    for (int i=0; i<tabRating2.size(); i++)
     {
        Glib::ustring ratingstring = tabRating2[i].c_str() ;
        ratinglisteDeroulantecol2.append(ratingstring) ;
     }



/* ********************************** */
    //Havens
    havensLabel.set_markup("-------------------------------------------------Havens--------------------------------------------------------") ;
    locationLabel.set_markup  ("Location") ;
    descriptionHavensLabel.set_markup  ("Description") ;

/* ********************************** */
    //Blood  Bonds/Vinculi
    historyLabel.set_markup("-------------------------------------------------History--------------------------------------------------------") ;
    preludeLabel.set_markup  ("Prelude") ;
    goalsLabel.set_markup  ("Goals") ;

/* ********************************** */
    ///Description

    descriptionDescriptionLabel.set_markup("-------------------------------------------------Description--------------------------------------------------------") ;
    descriptionAgeLabel.set_markup  ("Age") ;
    descriptionApparentAgeLabel.set_markup  ("Apparent Age") ;
    descriptionDateofBirthLabel.set_markup  ("Date of Birth") ;
    descriptionRIPLabel.set_markup  ("RIP") ;
    descriptionHairLabel.set_markup  ("Hair") ;
    descriptionEyesLabel.set_markup  ("Eyes") ;
    descriptionRaceLabel.set_markup  ("Race") ;
    descriptionNationalityLabel.set_markup  ("Nationality") ;
    descriptionHeightLabel.set_markup  ("Height") ;
    descriptionWeightLabel.set_markup  ("Weight") ;
    descriptionSexLabel.set_markup  ("Sex") ;


/* ********************************** */
    ///Visuals
    visualsLabel.set_markup("-------------------------------------------------Visuals--------------------------------------------------------") ;
    coterieChartLabel.set_markup  ("Coterie Chart ") ;
    characterSketchAgeLabel.set_markup  ("Character Sketch") ;



/* ********************************** */
    ///Rituals et Paths
    ritualsPathsLabel.set_markup("-------------------Rituals------------------------------------------------------------Paths--------------------------") ;
    ritualsNameLabel.set_markup  ("Rituals : Name") ;
    ritualsLevelLabel.set_markup  ("Level") ;
    pathsNameLabel.set_markup  ("Paths : Name") ;




/* ********************************** */
/* ********************************** */

    //utilisation des boites (Hbox et Vbox) qui sont des widgets pour afficher chaque elements

/* ********************************** */
    //La boite2 contient le Nom de l'utilisateur, le clan et la nature en liste déroulante
    
     boite2.set_spacing(10); // Espacer les widgets de 10
     boite2.pack_start(nameLabel,  Gtk::PACK_SHRINK) ;
     boite2.pack_start(nameEntry,  Gtk::PACK_SHRINK);
     boite2.pack_start(listeDeroulanteClanLabel,  Gtk::PACK_SHRINK) ;
     boite2.pack_start(listeDeroulanteClan,  Gtk::PACK_SHRINK) ;
     boite2.pack_start(listeDeroulanteNatureLabel,  Gtk::PACK_SHRINK) ;
     boite2.pack_start(listeDeroulanteNature,  Gtk::PACK_SHRINK) ;

/* ********************************** */
    //La boite3 contient les labels avec leur champs adéquates pour le joueur, demeaner et genetation
    
     boite3.set_spacing(10); // Espacer les widgets de 10
     boite3.pack_start(playerLabel, Gtk::PACK_SHRINK) ;
     boite3.pack_start(playerEntry,Gtk::PACK_SHRINK);
     boite3.pack_start(demeanerLabel, Gtk::PACK_SHRINK) ;
     boite3.pack_start(demeanerEntry,Gtk::PACK_SHRINK);
     boite3.pack_start(generationLabel, Gtk::PACK_SHRINK) ;
     boite3.pack_start(generationEntry,Gtk::PACK_SHRINK);

/* ********************************** */
    //La boite4 contient les labels avec leur champs adéquates pour chronide, concept et sire
     boite4.set_spacing(10); // Espacer les widgets de 10
     boite4.pack_start(chronideLabel, Gtk::PACK_SHRINK) ;
     boite4.pack_start(chronideEntry,Gtk::PACK_SHRINK);
     boite4.pack_start(conceptLabel, Gtk::PACK_SHRINK) ;
     boite4.pack_start(conceptEntry,Gtk::PACK_SHRINK);
     boite4.pack_start(sireLabel, Gtk::PACK_SHRINK) ;
     boite4.pack_start(sireEntry,Gtk::PACK_SHRINK);

     //les infos que utilisateur doit saisir sont dans deux boites (donc widgets) différents pour une question esthétique

/* ********************************** */
    //contient le Label permettant d'afficher la répartition des points aide a l'utilisateur
    boite_points.pack_start(pointsLabel); // Contient le text "Attributes: 7/5/3 • Abilities:13/9/5 • Disciplines:3 • Backgrounds:5 • Virtues:7"

/* ********************************** */
    /// ATTRIBUT qui correspond a la boite 5
    boite_attribut_.pack_start(attributLabel) ;

    // on va imposer une taille pour les attributs de physique pour voir toute les informations
    // car on a les info extrait du fichier et donc la description de chaque valeur
    //Listederoulante car cela va etre des Combo Text 
     //PHYSIQUE
     strengthlisteDeroulante.set_size_request(300);
     dexteritylisteDeroulante.set_size_request(300);
     staminalisteDeroulante.set_size_request(300);

    //SOCIAL
     charismalisteDeroulante.set_size_request(300);
     manipulationlisteDeroulante.set_size_request(300) ;
     apprearancelisteDeroulante.set_size_request(300);

    //MENTAL
     perceptionlisteDeroulante.set_size_request(300);
     intelligencelisteDeroulante.set_size_request(300) ;
     witslisteDeroulante.set_size_request(300);



    // La boite 5 va contenir les information des atributs de type physique uniquement
    // systeme de sous boite pour une quesiton de lisibilité

    // PARTIE AVEC ATTRIBUT PHYSIQUE
     boite511.pack_start(strengthLabel);
     boite511.pack_start(strengthlisteDeroulante);
     boite511.set_spacing(10);// Espacer les widgets de 10
     boite512.pack_start(dexterityLabel);
     boite512.pack_start(dexteritylisteDeroulante);
     boite512.set_spacing(10);
     boite513.pack_start(staminaLabel);
     boite513.pack_start(staminalisteDeroulante);
     boite513.set_spacing(10);
     boite51.pack_start(attributPhysique) ;
     boite51.pack_start(boite511) ;
     boite51.pack_start(boite512) ;
     boite51.pack_start(boite513) ;

     boite51.set_spacing(10);

    // PARTIE AVEC ATTRIBUT SOCIAL
     boite521.pack_start(charismaLabel);
     boite521.pack_start(charismalisteDeroulante);
     boite521.set_spacing(10);
     boite522.pack_start(manipulationLabel);
     boite522.pack_start(manipulationlisteDeroulante);
     boite522.set_spacing(10);
     boite523.pack_start(apprearanceLabel);
     boite523.pack_start(apprearancelisteDeroulante);
     boite523.set_spacing(10);
     boite52.pack_start(attributSocial) ;
     boite52.pack_start(boite521) ;
     boite52.pack_start(boite522) ;
     boite52.pack_start(boite523) ;

    // PARTIE AVEC ATTRIBUT MENTAL
     boite531.pack_start(perceptionLabel);
     boite531.pack_start(perceptionlisteDeroulante);
     boite531.set_spacing(10);
     boite532.pack_start(intelligenceLabel);
     boite532.pack_start(intelligencelisteDeroulante);
     boite532.set_spacing(10);
     boite533.pack_start(witsLabel);
     boite533.pack_start(witslisteDeroulante);
     boite533.set_spacing(10);
     boite53.pack_start(attributMental) ;
     boite53.pack_start(boite531) ;
     boite53.pack_start(boite532) ;
     boite53.pack_start(boite533);

     // inclu les sousboites dans la boite 5
     boite5.set_spacing(10); // Espacer les widgets de 10
     boite5.pack_start(boite51) ;
     boite5.pack_start(boite52) ;
     boite5.pack_start(boite53) ;

/* ********************************** */
    ///ABILITIE

    boite_abilities_.pack_start(abilitiesLabel) ;  // ---------- Abilities--------------
    //Abilities correspond a la boite 6 il est cnposer de Label et de Entry

    // PARTIE AVEC ABILITIE TALENT
     boite611.pack_start(alertnessLabel);
     boite611.pack_start(alertnessEntry);
     boite611.set_spacing(10);// Espacer les widgets de 10

     boite612.pack_start(athleticsLabel);
     boite612.pack_start(athleticsEntry);
     boite612.set_spacing(10);// Espacer les widgets de 10

     boite613.pack_start(awarenessLabel);
     boite613.pack_start(awarenessEntry);
     boite613.set_spacing(10);// Espacer les widgets de 10

     boite614.pack_start(brawlLabel);
     boite614.pack_start(brawlEntry);
     boite614.set_spacing(10);// Espacer les widgets de 10

     boite615.pack_start(empathyLabel);
     boite615.pack_start(empathyEntry);
     boite615.set_spacing(10);// Espacer les widgets de 10

     boite616.pack_start(expressionLabel);
     boite616.pack_start(expressionEntry);
     boite616.set_spacing(10);// Espacer les widgets de 10

     boite617.pack_start(intimidationLabel);
     boite617.pack_start(intimidationEntry);
     boite617.set_spacing(10);// Espacer les widgets de 10

     boite618.pack_start(leadershipLabel);
     boite618.pack_start(leadershipEntry);
     boite618.set_spacing(10);// Espacer les widgets de 10

     boite619.pack_start(streetwiseLabel);
     boite619.pack_start(streetwiseEntry);
     boite619.set_spacing(10);// Espacer les widgets de 10

     boite610.pack_start(subterfugeLabel);
     boite610.pack_start(subterfugeEntry);
     boite610.set_spacing(10);// Espacer les widgets de 10

     boite6101.pack_start(hobbyTalentLabel);
     boite6101.pack_start(hobbyTalentEntry);
     boite6101.set_spacing(10);// Espacer les widgets de 10



     boite61.pack_start(abilitiesTalents) ; //  "Talents"
     boite61.pack_start(boite611) ; // boite611 est vertical et  contient les alertness_____
     boite61.pack_start(boite612) ; // boite611 est vertical et  contient les Athletics_____
     boite61.pack_start(boite613) ; // boite611 est vertical et  contient les awareness_____
     boite61.pack_start(boite614) ; // boite611 est vertical et  contient les brawl
     boite61.pack_start(boite615) ; // boite611 est vertical et  contient les Empathy_____
     boite61.pack_start(boite616) ; // boite611 est vertical et  contient les expression
     boite61.pack_start(boite617) ; // boite611 est vertical et  contient les intimidation
     boite61.pack_start(boite618) ; // boite611 est vertical et  contient les leadership
     boite61.pack_start(boite619) ; // boite611 est vertical et  contient les streetwise
     boite61.pack_start(boite610) ; // boite611 est vertical et  contient les subterfuge
     boite61.pack_start(boite6101) ; // boite611 est vertical et  contient les hobbyTalent
     boite61.set_spacing(10);// Espacer les widgets de 10

    // PARTIE AVEC ABILITIE SKILLS
     boite621.pack_start(animalkenLabel);
     boite621.pack_start(animalkenEntry);
     boite621.set_spacing(10);// Espacer les widgets de 10

     boite622.pack_start(craftsLabel);
     boite622.pack_start(craftsEntry);
     boite622.set_spacing(10);// Espacer les widgets de 10

     boite623.pack_start(driveLabel);
     boite623.pack_start(driveEntry);
     boite623.set_spacing(10);// Espacer les widgets de 10

     boite624.pack_start(etiquetteLabel);
     boite624.pack_start(etiquetteEntry);
     boite624.set_spacing(10);// Espacer les widgets de 10

     boite625.pack_start(larcenyLabel);
     boite625.pack_start(larcenyEntry);
     boite625.set_spacing(10);// Espacer les widgets de 10

     boite626.pack_start(firearmsLabel);
     boite626.pack_start(firearmsEntry);
     boite626.set_spacing(10);// Espacer les widgets de 10

     boite627.pack_start(meleeLabel);
     boite627.pack_start(meleeEntry);
     boite627.set_spacing(10);// Espacer les widgets de 10

     boite628.pack_start(performanceLabel);
     boite628.pack_start(performanceEntry);
     boite628.set_spacing(10);// Espacer les widgets de 10

     boite629.pack_start(stealthLabel);
     boite629.pack_start(stealthEntry);
     boite629.set_spacing(10);// Espacer les widgets de 10

     boite620.pack_start(survivalLabel);
     boite620.pack_start(survivalEntry);
     boite620.set_spacing(10);// Espacer les widgets de 10

     boite6201.pack_start(professionalSkillLabel);
     boite6201.pack_start(professionalSkillEntry);
     boite6201.set_spacing(10);// Espacer les widgets de 10

     boite62.pack_start(abilitiesSkills) ; //  "Skills"
     boite62.pack_start(boite621) ; // boite621 est vertical et  contient les Animal Ken_____
     boite62.pack_start(boite622) ; // boite622 est vertical et  contient les Crafts_____
     boite62.pack_start(boite623) ; // boite623 est vertical et  contient les Dive_____
     boite62.pack_start(boite624) ; // boite623 est vertical et  contient les Etiquette_____
     boite62.pack_start(boite625) ; // boite623 est vertical et  contient les Firearms_____
     boite62.pack_start(boite626) ; // boite623 est vertical et  contient les Larceny_____
     boite62.pack_start(boite627) ; // boite623 est vertical et  contient les Melee_____
     boite62.pack_start(boite628) ; // boite623 est vertical et  contient les Performance_____
     boite62.pack_start(boite629) ; // boite623 est vertical et  contient les Stealth_____
     boite62.pack_start(boite620) ; // boite623 est vertical et  contient les Survival_____
     boite62.pack_start(boite6201) ; // boite623 est vertical et  contient les Professional Skill_____

    // PARTIE AVEC ABILITIE KNOWLEDGE
     boite631.pack_start(academicsLabel);
     boite631.pack_start(academicsEntry);
     boite631.set_spacing(10);// Espacer les widgets de 10

     boite632.pack_start(computerLabel);
     boite632.pack_start(computerEntry);
     boite632.set_spacing(10);// Espacer les widgets de 10

     boite633.pack_start(financeLabel);
     boite633.pack_start(financeEntry);
     boite633.set_spacing(10);// Espacer les widgets de 10

     boite634.pack_start(investigationLabel);
     boite634.pack_start(investigationEntry);
     boite634.set_spacing(10);// Espacer les widgets de 10

     boite635.pack_start(lawLabel);
     boite635.pack_start(lawEntry);
     boite635.set_spacing(10);// Espacer les widgets de 10

     boite636.pack_start(medicineLabel);
     boite636.pack_start(medicineEntry);
     boite636.set_spacing(10);// Espacer les widgets de 10

     boite637.pack_start(occultLabel);
     boite637.pack_start(occultEntry);
     boite637.set_spacing(10);// Espacer les widgets de 10

     boite638.pack_start(politicsLabel);
     boite638.pack_start(politicsEntry);
     boite638.set_spacing(10);// Espacer les widgets de 10

     boite639.pack_start(scienceLabel);
     boite639.pack_start(scienceEntry);
     boite639.set_spacing(10);// Espacer les widgets de 10

     boite630.pack_start(technologyLabel);
     boite630.pack_start(technologyEntry);
     boite630.set_spacing(10);// Espacer les widgets de 10

     boite6301.pack_start(expertLabel);
     boite6301.pack_start(expertEntry);
     boite6301.set_spacing(10);// Espacer les widgets de 10


     boite63.pack_start(abilitiesKnowledges) ; //  "Knowledges"
     boite63.pack_start(boite631) ; // boite631 est vertical
     boite63.pack_start(boite632) ; // boite632 est vertical
     boite63.pack_start(boite633) ;
     boite63.pack_start(boite634) ;
     boite63.pack_start(boite635) ;
     boite63.pack_start(boite636) ;
     boite63.pack_start(boite637) ;
     boite63.pack_start(boite638) ;
     boite63.pack_start(boite639) ;
     boite63.pack_start(boite630) ;
     boite63.pack_start(boite6301) ;

     // inclu les sousboites dans la boite 6
     boite6.set_spacing(10); // Espacer les widgets de 10
     boite6.pack_start(boite61) ;
     boite6.pack_start(boite62) ;
     boite6.pack_start(boite63) ;

/* ********************************** */

    ///ADVANTAGE
    //on va avoir 6 liste differente car on a 6 lignes correspondant

    //Listederoulante car cela va etre des Combo Text 
    //Dicspline
     discipline1listeDeroulante.set_size_request(300);
     discipline2listeDeroulante.set_size_request(300);
     discipline3listeDeroulante.set_size_request(300);
     discipline4listeDeroulante.set_size_request(300);
     discipline5listeDeroulante.set_size_request(300);
     discipline6listeDeroulante.set_size_request(300);

    //Background
     backgroud1listeDeroulante.set_size_request(300);
     backgroud2listeDeroulante.set_size_request(300);
     backgroud3listeDeroulante.set_size_request(300);
     backgroud4listeDeroulante.set_size_request(300);
     backgroud5listeDeroulante.set_size_request(300);
     backgroud6listeDeroulante.set_size_request(300);


    //Advantage se voit prendre la boite numero 7 
         boite_advantages_.pack_start(advantageLabel) ;  // "-------------------Advantage -------------------------------"
    //Dicipline
     boite711.pack_start(discipline1listeDeroulante);
     boite711.pack_start(discipline1AdvantageVal) ;
     boite712.pack_start(discipline2listeDeroulante);
     boite712.pack_start(discipline2AdvantageVal) ;
     boite713.pack_start(discipline3listeDeroulante);
     boite713.pack_start(discipline3AdvantageVal) ;
     boite714.pack_start(discipline4listeDeroulante);
     boite714.pack_start(discipline4AdvantageVal) ;
     boite715.pack_start(discipline5listeDeroulante);
     boite715.pack_start(discipline5AdvantageVal) ;
     boite716.pack_start(discipline6listeDeroulante);
     boite716.pack_start(discipline6AdvantageVal) ;

    //Backgrounds
     boite721.pack_start(backgroud1listeDeroulante);
     boite721.pack_start(backgroud1AdvantageVal);
     boite722.pack_start(backgroud2listeDeroulante);
     boite722.pack_start(backgroud2AdvantageVal);
     boite723.pack_start(backgroud3listeDeroulante);
     boite723.pack_start(backgroud3AdvantageVal);
     boite724.pack_start(backgroud4listeDeroulante);
     boite724.pack_start(backgroud4AdvantageVal);
     boite725.pack_start(backgroud5listeDeroulante);
     boite725.pack_start(backgroud5AdvantageVal);
     boite726.pack_start(backgroud6listeDeroulante);
     boite726.pack_start(backgroud6AdvantageVal);

    //pack tout les sous boite 
     boite71.pack_start(advantageDiscipline) ;
     boite71.pack_start(boite711) ;
     boite71.pack_start(boite712) ;
     boite71.pack_start(boite713) ;
     boite71.pack_start(boite714) ;
     boite71.pack_start(boite715) ;
     boite71.pack_start(boite716) ;

    //pack tout les sous boite 
     boite72.pack_start(advantageBackground) ;
     boite72.pack_start(boite721) ;
     boite72.pack_start(boite722) ;
     boite72.pack_start(boite723) ;
     boite72.pack_start(boite724) ;
     boite72.pack_start(boite725) ;
     boite72.pack_start(boite726) ;


    //pack tout les sous boite 
     boite731.pack_start(virtue1Label);
     boite731.pack_start(virtue1);
     boite731.set_spacing(10);
     boite732.pack_start(virtue2Label);
     boite732.pack_start(virtue2);
     boite732.set_spacing(10);
     boite733.pack_start(virtue3Label);
     boite733.pack_start(virtue3);
     boite732.set_spacing(10);

    //pack tout les sous boite 
     boite73.pack_start(advantageVirtue) ;
     boite73.pack_start(boite731) ;
     boite73.pack_start(boite732) ;
     boite73.pack_start(boite733) ;

    //pack tout les sous boite a la boite7 final
     boite7.set_spacing(10);
     boite7.pack_start(boite71) ;
     boite7.pack_start(boite72) ;
     boite7.pack_start(boite73) ;


/* ********************************** */
    ///PARTIE APRES ADVANTAGE

    //HEALTH
    //Listederoulante car cela va etre des Combo Text 
    //obliger de declarer des variable differente a chaque fois car info qu'on a stocket dans le ficher n'est jamais la meme
    healthlisteDeroulanteBruised.set_size_request(30);
    healthlisteDeroulanteHurt.set_size_request(30);
    healthlisteDeroulanteInjured.set_size_request(30);
    healthlisteDeroulanteWounded.set_size_request(30);
    healthlisteDeroulanteMauled.set_size_request(30);
    healthlisteDeroulanteCrippled.set_size_request(30);
    healthlisteDeroulanteIncapacitated.set_size_request(30);

    //Cette partie se pack dans la boite 8
    boite_transition_.pack_start(transitionLabel); // -----------------------
    boite811.pack_start(tapeHereLabel); //Tape Here
    boite812.pack_start(typeHereEntry); // text a saisir

    boite81.pack_start(boite811);
    boite81.pack_start(boite812);

    //pack tout les sous boite 
    boite821.set_spacing(10); // Espacer les widgets de 10
    boite821.pack_start(humanityPathLabel); //Humanity
    boite822.pack_start(humanityPathEntry); // text a saisir
    boite822.set_spacing(10);

    boite823.pack_start(willpowerLabel); //willpower
    boite824.pack_start(willpowerEntry);// text a saisir
    boite824.set_spacing(10);


    boite825.pack_start(bloodPoolLabel);//bloodPool
    boite826.pack_start(bloodPoolEntry); // text a saisir
    boite826.set_spacing(10);

    //pack tout les sous boite 
    boite82.pack_start(boite821);
    boite82.pack_start(boite822);
    boite82.pack_start(boite823);
    boite82.pack_start(boite824);
    boite82.pack_start(boite825);
    boite82.pack_start(boite826);

    //pack tout les sous boite 
    boite83.set_spacing(10); // Espacer les widgets de 10
    boite83.pack_start(healthPathLabel);
    boite831.pack_start(bruisedLabel);
    boite831.pack_start(healthlisteDeroulanteBruised);

    boite832.pack_start(hurtLabel);
    boite832.pack_start(healthlisteDeroulanteHurt);

    boite833.pack_start(injuredLabel);
    boite833.pack_start(healthlisteDeroulanteInjured);

    boite834.pack_start(woundedLabel);
    boite834.pack_start(healthlisteDeroulanteWounded);

    boite835.pack_start(mauledLabel);
    boite835.pack_start(healthlisteDeroulanteMauled);

    boite836.pack_start(crippledLabel);
    boite836.pack_start(healthlisteDeroulanteCrippled);

    boite837.pack_start(incapacitatedLabel);
    boite837.pack_start(healthlisteDeroulanteIncapacitated);
    boite837.set_spacing(10);// Espacer les widgets de 10

    boite838.set_spacing(10);// Espacer les widgets de 10
    boite838.pack_start(weaknessLabel);
    boite839.pack_start(weaknessEntry);

    boite830.pack_start(experienceLabel);
    boite8301.pack_start(experienceEntry);

    //pack tout les sous boite 
    boite83.pack_start(boite831);
    boite83.pack_start(boite832);
    boite83.pack_start(boite833);
    boite83.pack_start(boite834);
    boite83.pack_start(boite835);
    boite83.pack_start(boite836);
    boite83.pack_start(boite837);
    boite83.pack_start(boite838);
    boite83.pack_start(boite839);
    boite83.pack_start(boite830);
    boite83.pack_start(boite8301);

    //pack toute les boites dans la boite 8
    boite8.set_spacing(10);
    boite8.pack_start(boite81) ;
    boite8.pack_start(boite82) ;
    boite8.pack_start(boite83) ;

/* ********************************** */
    /// MERITS AND FLAWS

     boite_meritsANDflaws_.pack_start(meritANDflawLabel) ;

     //Cette partie est inclu dans la boite 9
    // a chaque fois on va avoir un Labele et une liste deroulante c'est a dire un combo Text
    //on reconne en colonne chaque block est une colonne et il y a 7 a chaque fois hors Label) car c'est les 7 lignes 
     // Merit
     boite91.pack_start(MeritLabel) ;
     boite91.pack_start(merit1listeDeroulante) ;
     boite91.pack_start(merit2listeDeroulante) ;
     boite91.pack_start(merit3listeDeroulante) ;
     boite91.pack_start(merit4listeDeroulante) ;
     boite91.pack_start(merit5listeDeroulante) ;
     boite91.pack_start(merit6listeDeroulante) ;
     boite91.pack_start(merit7listeDeroulante) ;

    // Merit Type
     boite92.pack_start(meritTypeLabel) ;
     boite92.pack_start(meritType1listeDeroulante) ;
     boite92.pack_start(meritType2listeDeroulante) ;
     boite92.pack_start(meritType3listeDeroulante) ;
     boite92.pack_start(meritType4listeDeroulante) ;
     boite92.pack_start(meritType5listeDeroulante) ;
     boite92.pack_start(meritType6listeDeroulante) ;
     boite92.pack_start(meritType7listeDeroulante) ;

    //Merit cost 
     boite93.pack_start(CostMeritLabel) ;
     boite93.pack_start(meritCost1listeDeroulante) ;
     boite93.pack_start(meritCost2listeDeroulante) ;
     boite93.pack_start(meritCost3listeDeroulante) ;
     boite93.pack_start(meritCost4listeDeroulante) ;
     boite93.pack_start(meritCost5listeDeroulante) ;
     boite93.pack_start(meritCost6listeDeroulante) ;
     boite93.pack_start(meritCost7listeDeroulante) ;

    //Flaws
     boite94.pack_start(FlawLabel) ;
     boite94.pack_start(flow1listeDeroulante) ;
     boite94.pack_start(flow2listeDeroulante) ;
     boite94.pack_start(flow3listeDeroulante) ;
     boite94.pack_start(flow4listeDeroulante) ;
     boite94.pack_start(flow5listeDeroulante) ;
     boite94.pack_start(flow6listeDeroulante) ;
     boite94.pack_start(flow7listeDeroulante) ;

    //Flaws Type
     boite95.pack_start(flawTypeLabel) ;
     boite95.pack_start(flowType1listeDeroulante) ;
     boite95.pack_start(flowType2listeDeroulante) ;
     boite95.pack_start(flowType3listeDeroulante) ;
     boite95.pack_start(flowType4listeDeroulante) ;
     boite95.pack_start(flowType5listeDeroulante) ;
     boite95.pack_start(flowType6listeDeroulante) ;
     boite95.pack_start(flowType7listeDeroulante) ;

    // Flaws Bonus (la dernere colonne)
     boite96.pack_start(BonusLabel) ;
     boite96.pack_start(bonus1listeDeroulante) ;
     boite96.pack_start(bonus2listeDeroulante) ;
     boite96.pack_start(bonus3listeDeroulante) ;
     boite96.pack_start(bonus4listeDeroulante) ;
     boite96.pack_start(bonus5listeDeroulante) ;
     boite96.pack_start(bonus6listeDeroulante) ;
     boite96.pack_start(bonus7listeDeroulante) ;

    //pack tout les sous boite 
     boite9.set_spacing(10);
     boite9.pack_start(boite91) ;
     boite9.pack_start(boite92) ;
     boite9.pack_start(boite93) ;
     boite9.pack_start(boite94) ;
     boite9.pack_start(boite95) ;
     boite9.pack_start(boite96) ;


/* ********************************** */
    /// partie  Other Traits

    //Le Label principal
    boite_otherTraits_.pack_start(otherTraitsLabel) ;  // ---------- Other Traits--------------

    ///Prend la boite 10
    //Premiere Colonne
    boite1011.pack_start(otherTraits1Entry);
    boite1011.pack_start(listeDeroulanteOtherTraits1);
    boite1011.set_spacing(10);// Espacer les widgets de 10

    boite101.pack_start(boite1011) ; // boite101 est vertical

    //Deuxieme Colonne
    boite1022.pack_start(otherTraits2Entry);
    boite1022.pack_start(listeDeroulanteOtherTraits2);
    boite1022.set_spacing(10);// Espacer les widgets de 10

    boite102.pack_start(boite1022) ; // boite102 est vertical


    //Troisieme Colonne
    boite1033.pack_start(otherTraits3Entry);
    boite1033.pack_start(listeDeroulanteOtherTraits3);
    boite1033.set_spacing(10);// Espacer les widgets de 10

    boite103.pack_start(boite1033) ; // boite103 est vertical


    // inclu les sousboites dans la boite 10
     boite10.set_spacing(10); // Espacer les widgets de 10
     boite10.pack_start(boite101) ;
     boite10.pack_start(boite102) ;
     boite10.pack_start(boite103) ;



/********************************************************/
    //Rituals et Paths
    //Le Label principal
   boite_ritualsPaths_.pack_start(ritualsPathsLabel) ;

    //C'est la boite 21 mais elle est place au bon endroit selon le pdf modele 
    // Premiere Colonne
     boite2111.pack_start(ritualsNameLabel);
     boite2111.set_spacing(10);// Espacer les widgets de 10

     boite2111.pack_start(ritualsLevelLabel);
     boite2111.set_spacing(10);// Espacer les widgets de 10

     boite2112.pack_start(listeDeroulanteritualsName);
     boite2112.set_spacing(10);// Espacer les widgets de 10

     boite2112.pack_start(listeDeroulanteritualsLevel);
     boite2112.set_spacing(10);// Espacer les widgets de 10

    //pack tout les sous boite 
     boite211.pack_start(boite2111) ;
     boite211.pack_start(boite2112) ;


    //Deuxieme Colonne
     boite2121.pack_start(pathsNameLabel);
     boite2121.set_spacing(10);// Espacer les widgets de 10


     boite2122.pack_start(listeDeroulantepathsName);
     boite2122.set_spacing(10);// Espacer les widgets de 10

     boite2122.pack_start(listeDeroulantepathsLevel);
     boite2122.set_spacing(10);// Espacer les widgets de 10

    //pack tout les sous boite 
     boite212.pack_start(boite2121) ;
     boite212.pack_start(boite2122) ;



     // inclu les sousboites dans la boite 21
     boite21.set_spacing(10); // Espacer les widgets de 10
     boite21.pack_start(boite211) ;
     boite21.pack_start(boite212) ;





/* ********************************** */
    // Experience Derangements

    //Le Label principal
    boite_experienceDerangements_.pack_start(experienceDerangementsLabel) ;

    //Cette section prend lea boite numero 12
    
    //pack tout les sous boite 
    boite1200.pack_start(experienceDLabel);
    boite1200.set_spacing(10);// Espacer les widgets de 10

    boite1201.pack_start(totalLabel);
    boite1201.set_spacing(10);// Espacer les widgets de 10

    boite1201.pack_start(totalEntry);
    boite1201.set_spacing(10);// Espacer les widgets de 10

    boite1202.pack_start(totalSpentLabel);
    boite1202.set_spacing(10);// Espacer les widgets de 10

    boite1202.pack_start(totalSpentEntry);
    boite1202.set_spacing(10);// Espacer les widgets de 10

    boite1203.pack_start(spentOnLabel);
    boite1203.set_spacing(10);// Espacer les widgets de 10

    boite1204.pack_start(spentOnEntry);
    boite1204.set_spacing(10);// Espacer les widgets de 10

    //pack tout les sous boite 
    boite121.pack_start(boite1200) ;
    boite121.pack_start(boite1201) ;
    boite121.pack_start(boite1202) ;
    boite121.pack_start(boite1203) ;
    boite121.pack_start(boite1204) ;

    //pack tout les sous boite 
    boite1220.pack_start(derangementsLabel);
    boite1220.set_spacing(10);// Espacer les widgets de 10


    boite1221.pack_start(derangementsEntry);
    boite1221.set_spacing(10);// Espacer les widgets de 10

    boite122.pack_start(boite1220) ;
    boite122.pack_start(boite1221) ;


    // inclu les sousboites dans la boite 12
     boite12.set_spacing(10); // Espacer les widgets de 10
     boite12.pack_start(boite121) ;
     boite12.set_spacing(10); // Espacer les widgets de 10
     boite12.pack_start(boite122) ;

/* ********************************** */
    ///Combat

    //Le Label principal
   boite_combat_.pack_start(combatLabel) ;


    // Premiere Colonne
     boite1311.pack_start(combatWeaponAttackLabel);
     boite1311.set_spacing(10);// Espacer les widgets de 10
     boite1312.pack_start(combatWeaponAttackEntry);
     boite1312.set_spacing(10);// Espacer les widgets de 10

     boite1311.pack_start(combatDiffLabel);
     boite1311.set_spacing(10);// Espacer les widgets de 10
     boite1312.pack_start(combatDiffEntry);
     boite1312.set_spacing(10);// Espacer les widgets de 10

     boite1311.pack_start(combatDamageLabel);
     boite1311.set_spacing(10);// Espacer les widgets de 10
     boite1312.pack_start(combatDamageEntry);
     boite1312.set_spacing(10);// Espacer les widgets de 10

     boite1311.pack_start(combatRangeLabel);
     boite1311.set_spacing(10);// Espacer les widgets de 10
     boite1312.pack_start(combatRangeEntry);
     boite1312.set_spacing(10);// Espacer les widgets de 10

     boite1311.pack_start(combatRateLabel);
     boite1311.set_spacing(10);// Espacer les widgets de 10
     boite1312.pack_start(combatRateEntry);
     boite1312.set_spacing(10);// Espacer les widgets de 10

     boite1311.pack_start(combatClipLabel);
     boite1311.set_spacing(10);// Espacer les widgets de 10
     boite1312.pack_start(combatClipEntry);
     boite1312.set_spacing(10);// Espacer les widgets de 10

     boite1311.pack_start(combatConcealLabel);
     boite1311.set_spacing(10);// Espacer les widgets de 10
     boite1312.pack_start(combatConcealEntry);
     boite1312.set_spacing(10);// Espacer les widgets de 10

     boite131.pack_start(boite1311) ;
     boite131.pack_start(boite1312) ;

    //Deuxieme Colonne
     boite1321.pack_start(armorArmorLabel);
     boite1321.set_spacing(10);// Espacer les widgets de 10

     boite1322.pack_start(armorClassLabel);
     boite1322.set_spacing(10);// Espacer les widgets de 10
     boite1322.pack_start(armorClassEntry);
     boite1322.set_spacing(10);// Espacer les widgets de 10

     boite1323.pack_start(armorRatingLabel);
     boite1323.set_spacing(10);// Espacer les widgets de 10
     boite1323.pack_start(armorRatingEntry);
     boite1323.set_spacing(10);// Espacer les widgets de 10

     boite1324.pack_start(armorPenaltyLabel);
     boite1324.set_spacing(10);// Espacer les widgets de 10
     boite1324.pack_start(armorPenaltyEntry);
     boite1324.set_spacing(10);// Espacer les widgets de 10

     boite1325.pack_start(armorDescriptionLabel);
     boite1325.set_spacing(10);// Espacer les widgets de 10


     boite1326.pack_start(armorDescriptionEntry);
     boite1326.set_spacing(10);// Espacer les widgets de 10

    //pack tout les sous boite 
     boite132.pack_start(boite1321) ;
     boite132.pack_start(boite1322) ;
     boite132.pack_start(boite1323) ;
     boite132.pack_start(boite1324) ;
     boite132.pack_start(boite1325) ;
     boite132.pack_start(boite1326) ;


     // inclu les sousboites dans la boite 13
     boite13.set_spacing(10); // Espacer les widgets de 10
     boite13.pack_start(boite131) ;
     boite13.pack_start(boite132) ;


/* ********************************** */
    ///Expanded Backgrounds

    //Le Label principal
   boite_expandedBackgrounds_.pack_start(expandedBackgroundsLabel) ;


    // Expanded Background pour la boite 14

    // Premiere Colonne
     boite1411.pack_start(alliesLabel);
     boite1411.set_spacing(10);// Espacer les widgets de 10

     boite1412.pack_start(alliesEntry);
     boite1412.set_spacing(10);// Espacer les widgets de 10

     boite1413.pack_start(contactsLabel);
     boite1413.set_spacing(10);// Espacer les widgets de 10

     boite1414.pack_start(contactsEntry);
     boite1414.set_spacing(10);// Espacer les widgets de 10

     boite1415.pack_start(fameLabel);
     boite1415.set_spacing(10);// Espacer les widgets de 10

     boite1416.pack_start(fameEntry);
     boite1416.set_spacing(10);// Espacer les widgets de 10

     boite1417.pack_start(herdLabel);
     boite1417.set_spacing(10);// Espacer les widgets de 10

     boite1418.pack_start(herdEntry);
     boite1418.set_spacing(10);// Espacer les widgets de 10

     boite1419.pack_start(influenceLabel);
     boite1419.set_spacing(10);// Espacer les widgets de 10

     boite1410.pack_start(influenceEntry);
     boite1410.set_spacing(10);// Espacer les widgets de 10

    //pack tout les sous boite 
     boite141.pack_start(boite1411) ;
     boite141.pack_start(boite1412) ;
     boite141.pack_start(boite1411) ;
     boite141.pack_start(boite1412) ;
     boite141.pack_start(boite1413) ;
     boite141.pack_start(boite1414) ;
     boite141.pack_start(boite1415) ;
     boite141.pack_start(boite1416) ;
     boite141.pack_start(boite1417) ;
     boite141.pack_start(boite1418) ;
     boite141.pack_start(boite1419) ;
     boite141.pack_start(boite1410) ;

    //Deuxieme Colonne
     boite1421.pack_start(mentorLabel);
     boite1421.set_spacing(10);// Espacer les widgets de 10

     boite1422.pack_start(mentorEntry);
     boite1422.set_spacing(10);// Espacer les widgets de 10

     boite1423.pack_start(resourcesLabel);
     boite1423.set_spacing(10);// Espacer les widgets de 10

     boite1424.pack_start(resourcesEntry);
     boite1424.set_spacing(10);// Espacer les widgets de 10

     boite1425.pack_start(retainersLabel);
     boite1425.set_spacing(10);// Espacer les widgets de 10

     boite1426.pack_start(retainersEntry);
     boite1426.set_spacing(10);// Espacer les widgets de 10

     boite1427.pack_start(statusLabel);
     boite1427.set_spacing(10);// Espacer les widgets de 10

     boite1428.pack_start(statusEntry);
     boite1428.set_spacing(10);// Espacer les widgets de 10

     boite1429.pack_start(otherLabel);
     boite1429.set_spacing(10);// Espacer les widgets de 10

     boite1429.pack_start(other1listeDeroulante);
     boite1429.set_spacing(10);// Espacer les widgets de 10

     boite1420.pack_start(other2Entry);
     boite1420.set_spacing(10);// Espacer les widgets de 10

    //pack tout les sous boite 
     boite142.pack_start(boite1421) ;
     boite142.pack_start(boite1422) ;
     boite142.pack_start(boite1423) ;
     boite142.pack_start(boite1424) ;
     boite142.pack_start(boite1425) ;
     boite142.pack_start(boite1426) ;
     boite142.pack_start(boite1427) ;
     boite142.pack_start(boite1428) ;
     boite142.pack_start(boite1429) ;
     boite142.pack_start(boite1420) ;


     // inclu les sousboites dans la boite 14
     boite14.set_spacing(10); // Espacer les widgets de 10
     boite14.pack_start(boite141) ;
     boite14.pack_start(boite142) ;

/* ********************************** */
    ///Possessions

    //Le Label principal
    boite_possessions_.pack_start(possessionsLabel) ;

    // Possession possede la boite 15

    // Premiere Colonne
     boite1511.pack_start(gearLabel);
     boite1511.set_spacing(10);// Espacer les widgets de 10

     boite1512.pack_start(gearEntry);
     boite1512.set_spacing(10);// Espacer les widgets de 10

     boite1513.pack_start(feedingGroundsLabel);
     boite1513.set_spacing(10);// Espacer les widgets de 10

     boite1514.pack_start(feedingGroundsEntry);
     boite1514.set_spacing(10);// Espacer les widgets de 10

    //pack tout les sous boite 
     boite151.pack_start(boite1511) ;
     boite151.pack_start(boite1512) ;
     boite151.pack_start(boite1513) ;
     boite151.pack_start(boite1514) ;


    //Deuxieme Colonne
     boite1521.pack_start(equipmentLabel);
     boite1521.set_spacing(10);// Espacer les widgets de 10

     boite1522.pack_start(equipmentEntry);
     boite1522.set_spacing(10);// Espacer les widgets de 10

     boite1523.pack_start(vehiclesLabel);
     boite1523.set_spacing(10);// Espacer les widgets de 10

     boite1524.pack_start(vehiclesEntry);
     boite1524.set_spacing(10);// Espacer les widgets de 10

    //pack tout les sous boite 
     boite152.pack_start(boite1521) ;
     boite152.pack_start(boite1522) ;
     boite152.pack_start(boite1523) ;
     boite152.pack_start(boite1524) ;


     // inclu les sousboites dans la boite 15
     boite15.set_spacing(10); // Espacer les widgets de 10
     boite15.pack_start(boite151) ;
     boite15.pack_start(boite152) ;

/* ********************************** */
    ///Blood   Bonds/Vinculi

    //Le Label principal
   boite_bloodBondsVinculi_.pack_start(bloodBondsVinculiLabel) ;

    //Cette section prend la boite 16

    // Premiere Colonne
     boite1611.pack_start(boundToLabelcol1);
     boite1611.set_spacing(10);// Espacer les widgets de 10

     boite1612.pack_start(boundToEntrycol1);
     boite1612.set_spacing(10);// Espacer les widgets de 10

     boite1611.pack_start(ratingLabelcol1);
     boite1611.set_spacing(10);// Espacer les widgets de 10

     boite1612.pack_start(ratinglisteDeroulantecol1);
     boite1612.set_spacing(10);// Espacer les widgets de 10

     boite161.pack_start(boite1611) ;
     boite161.pack_start(boite1612) ;


    //Deuxieme Colonne
     boite1621.pack_start(boundToLabelcol2);
     boite1621.set_spacing(10);// Espacer les widgets de 10

     boite1622.pack_start(boundToEntrycol2);
     boite1622.set_spacing(10);// Espacer les widgets de 10

     boite1621.pack_start(ratingLabelcol2);
     boite1621.set_spacing(10);// Espacer les widgets de 10

     boite1622.pack_start(ratinglisteDeroulantecol2);
     boite1622.set_spacing(10);// Espacer les widgets de 10

     boite162.pack_start(boite1621) ;
     boite162.pack_start(boite1622) ;


     // inclu les sousboites dans la boite 16
     boite16.set_spacing(10); // Espacer les widgets de 10
     boite16.pack_start(boite161) ;
     boite16.pack_start(boite162) ;

/* ********************************** */
    ///Havens

    //Le Label principal
   boite_havens_.pack_start(havensLabel) ;

    //On est sur la boite 17 pour Heavens

    // Premiere Colonne
     boite1711.pack_start(locationLabel);
     boite1711.set_spacing(10);// Espacer les widgets de 10

     boite1712.pack_start(locationEntry);
     boite1712.set_spacing(10);// Espacer les widgets de 10

     boite171.pack_start(boite1711) ;
     boite171.pack_start(boite1712) ;

    //Deuxieme Colonne
     boite1721.pack_start(descriptionHavensLabel);
     boite1721.set_spacing(10);// Espacer les widgets de 10

     boite1722.pack_start(descriptionHavensEntry);
     boite1722.set_spacing(10);// Espacer les widgets de 10


     boite172.pack_start(boite1721) ;
     boite172.pack_start(boite1722) ;

     // inclu les sousboites dans la boite 17
     boite17.set_spacing(10); // Espacer les widgets de 10
     boite17.pack_start(boite171) ;
     boite17.pack_start(boite172) ;

/* ********************************** */
    ///History

    //Le Label principal
    boite_history_.pack_start(historyLabel) ;

    //History prend la boite 18 

    // Premiere Colonne
     boite1811.pack_start(preludeLabel);
     boite1811.set_spacing(10);// Espacer les widgets de 10

     boite1812.pack_start(preludeEntry);
     boite1812.set_spacing(10);// Espacer les widgets de 10

     boite1813.pack_start(goalsLabel);
     boite1813.set_spacing(10);// Espacer les widgets de 10

     boite1814.pack_start(goalsLabel);
     boite1814.set_spacing(10);// Espacer les widgets de 10

     boite181.pack_start(boite1811) ;
     boite181.pack_start(boite1812) ;
     boite181.pack_start(boite1813) ;
     boite181.pack_start(boite1814) ;

     // inclu les sousboites dans la boite 18
     boite18.set_spacing(10); // Espacer les widgets de 10
     boite18.pack_start(boite181) ;

/* ********************************** */
    ///Description

        //Le Label principal
    boite_description_.pack_start(descriptionDescriptionLabel) ;

    // Description prend la boite 19

    // Premiere Colonne
     boite1911.pack_start(descriptionAgeLabel);
     boite1911.set_spacing(10);// Espacer les widgets de 10

     boite1911.pack_start(descriptionAgeEntry);
     boite1911.set_spacing(10);// Espacer les widgets de 10

     boite1912.pack_start(descriptionApparentAgeLabel);
     boite1912.set_spacing(10);// Espacer les widgets de 10

     boite1912.pack_start(descriptionApparentAgeEntry);
     boite1912.set_spacing(10);// Espacer les widgets de 10

     boite1913.pack_start(descriptionDateofBirthLabel);
     boite1913.set_spacing(10);// Espacer les widgets de 10

     boite1913.pack_start(descriptionDateofBirthEntry);
     boite1913.set_spacing(10);// Espacer les widgets de 10

     boite1914.pack_start(descriptionRIPLabel);
     boite1914.set_spacing(10);// Espacer les widgets de 10

     boite1914.pack_start(descriptionRIPEntry);
     boite1914.set_spacing(10);// Espacer les widgets de 10

     boite1915.pack_start(descriptionHairLabel);
     boite1915.set_spacing(10);// Espacer les widgets de 10

     boite1915.pack_start(descriptionHairEntry);
     boite1915.set_spacing(10);// Espacer les widgets de 10

     boite1916.pack_start(descriptionEyesLabel);
     boite1916.set_spacing(10);// Espacer les widgets de 10

     boite1916.pack_start(descriptionEyesEntry);
     boite1916.set_spacing(10);// Espacer les widgets de 10


     boite1917.pack_start(descriptionRaceLabel);
     boite1917.set_spacing(10);// Espacer les widgets de 10

     boite1917.pack_start(descriptionRaceEntry);
     boite1917.set_spacing(10);// Espacer les widgets de 10

     boite1918.pack_start(descriptionNationalityLabel);
     boite1918.set_spacing(10);// Espacer les widgets de 10

     boite1918.pack_start(descriptionNationalityEntry);
     boite1918.set_spacing(10);// Espacer les widgets de 10

     boite1919.pack_start(descriptionHeightLabel);
     boite1919.set_spacing(10);// Espacer les widgets de 10

     boite1919.pack_start(descriptionHeightEntry);
     boite1919.set_spacing(10);// Espacer les widgets de 10

     boite1910.pack_start(descriptionWeightLabel);
     boite1910.set_spacing(10);// Espacer les widgets de 10

     boite1910.pack_start(descriptionWeightEntry);
     boite1910.set_spacing(10);// Espacer les widgets de 10

     boite19101.pack_start(descriptionSexLabel);
     boite19101.set_spacing(10);// Espacer les widgets de 10

     boite19101.pack_start(descriptionSexEntry);
     boite19101.set_spacing(10);// Espacer les widgets de 10

    //pack tout les sous boite 
     boite191.pack_start(boite1911) ;
     boite191.pack_start(boite1912) ;
     boite191.pack_start(boite1913) ;
     boite191.pack_start(boite1914) ;
     boite191.pack_start(boite1915) ;
     boite191.pack_start(boite1916) ;
     boite191.pack_start(boite1917) ;
     boite191.pack_start(boite1918) ;
     boite191.pack_start(boite1919) ;
     boite191.pack_start(boite1910) ;
     boite191.pack_start(boite19101) ;


    //Deuxieme Colonne
     boite1921.pack_start(descriptionAgeEntry2);
     boite1921.set_spacing(10);// Espacer les widgets de 10

     boite1922.pack_start(descriptionApparentAgeEntry2);
     boite1922.set_spacing(10);// Espacer les widgets de 10

     boite1923.pack_start(descriptionDateofBirthEntry2);
     boite1923.set_spacing(10);// Espacer les widgets de 10

     boite1924.pack_start(descriptionRIPEntry2);
     boite1924.set_spacing(10);// Espacer les widgets de 10

     boite1925.pack_start(descriptionHairEntry2);
     boite1925.set_spacing(10);// Espacer les widgets de 10

     boite1926.pack_start(descriptionEyesEntry2);
     boite1926.set_spacing(10);// Espacer les widgets de 10

     boite1927.pack_start(descriptionRaceEntry2);
     boite1927.set_spacing(10);// Espacer les widgets de 10

     boite1928.pack_start(descriptionNationalityEntry2);
     boite1928.set_spacing(10);// Espacer les widgets de 10

     boite1929.pack_start(descriptionHeightEntry2);
     boite1929.set_spacing(10);// Espacer les widgets de 10

     boite1920.pack_start(descriptionWeightEntry2);
     boite1920.set_spacing(10);// Espacer les widgets de 10

     boite19201.pack_start(descriptionSexEntry2);
     boite19201.set_spacing(10);// Espacer les widgets de 10

    //pack tout les sous boite 
     boite192.pack_start(boite1921) ;
     boite192.pack_start(boite1922) ;
     boite192.pack_start(boite1923) ;
     boite192.pack_start(boite1924) ;
     boite192.pack_start(boite1925) ;
     boite192.pack_start(boite1926) ;
     boite192.pack_start(boite1927) ;
     boite192.pack_start(boite1928) ;
     boite192.pack_start(boite1929) ;
     boite192.pack_start(boite1920) ;
     boite192.pack_start(boite19201) ;

     // inclu les sousboites dans la boite 19
     boite19.set_spacing(10); // Espacer les widgets de 10
     boite19.pack_start(boite191) ;
     boite19.pack_start(boite192) ;

/* ********************************** */
    ///Visuals

    //Le Label principal
   boite_visuals_.pack_start(visualsLabel) ;

    //La derniere sections prend la boite 20
    // Premiere Colonne
     boite2011.pack_start(coterieChartLabel);
     boite2011.set_spacing(10);// Espacer les widgets de 10


    //Deuxieme Colonne
     boite2011.pack_start(characterSketchAgeLabel);
     boite2011.set_spacing(10);// Espacer les widgets de 10

    boite201.pack_start(boite2011) ;

     // inclu les sousboites dans la boite 20
     boite20.set_spacing(10); // Espacer les widgets de 10
     boite20.pack_start(boite201) ;


/* ********************************** */

    // les labels pour les boutons valider les information saison par utilisateur et quitter l'application
    // le bouton va servir pour valider la fenetre
     boutonValider.set_label("Valider") ;
     boutonQuitter.set_label("Quitter") ;

    // boite speciale pour les boutons valider et quitter 
    // creation d'un widget boitePiedPage qui va afficher les bonutons valider et quitter et gerer les positions
    boitePiedPage.set_layout(Gtk::BUTTONBOX_CENTER) ;
    boitePiedPage.set_spacing(120);
    boitePiedPage.pack_start(boutonQuitter,Gtk::PACK_SHRINK) ;
    boitePiedPage.pack_start(boutonValider, Gtk::PACK_SHRINK) ;



/* ********************************** */

    // inclu toute les autre boite de type horizontale qui sont des widgets  la boite 1 (de type verticale) contient toutes les autres
    // Pack toute les boites sur la boite 1
    boite1.pack_start(boite2, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite3, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite4, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_points, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_attribut_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite5, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_abilities_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite6, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_advantages_) ;
    boite1.pack_start(boite7, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_transition_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite8, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_meritsANDflaws_) ;
    boite1.pack_start(boite9, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_otherTraits_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite10, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_ritualsPaths_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite21, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_experienceDerangements_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite12, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_combat_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite13, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_expandedBackgrounds_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite14, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_possessions_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite15, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_bloodBondsVinculi_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite16, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_havens_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite17, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_history_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite18, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_description_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite19, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite_visuals_, Gtk::PACK_SHRINK) ;
    boite1.pack_start(boite20, Gtk::PACK_SHRINK) ;

    boite1.pack_end(boitePiedPage) ;
    boite1.set_spacing(10);  // Ajouter un espace entre les HBox

    //Permet de faire la barre de navigation
    barresDeDefefilement.add(boite1);

    //add(boite1) ; //on commente cette ligne car on a ajouter la barre de defilement c'est elle qui va etre add
    add(barresDeDefefilement) ;

    //permet d'afficher tout les widgets
    show_all() ;

    //signal_clicked va permettre de relier les boutons a des methodes
    //grace a   signal clicked() appel de la fonction Gtk::Main::quit() qui ferme le programme.
    boutonQuitter.signal_clicked().connect(sigc::ptr_fun(&Gtk::Main::quit)) ;

    // Appel de la fonction de validation quand utilisateur en clique  sur le bouton Valider
    boutonValider.signal_clicked().connect(sigc::mem_fun(*this, &Fenetre::validationDeLaSaisie )) ;
}



// fonction de type bool qui va lire  le fichier de validation
bool Fenetre::lecturefichierV (Glib::ustring nomSaisiGet)
{

    //initialise la variable a faux on part du principe que le nom n'est pas présent
    bool trouveDansFichierV = false ;
    //gestion du fichier en mode lecture
    ifstream fichierEntree("fichierV.txt") ;
    if (fichierEntree)
    {
        //variable de type string qui correspond a chaque ligne du fichier
         string  ligneFichierV ;

        while (getline(fichierEntree, ligneFichierV))
        {
            if (ligneFichierV.substr(0, 6) == "--Name")
            {
                //si le fichier possede deja des donnée saisie (car quand utilisateur va taper son nom dans fichier le fichier va  signaler que c'est le nom via "-- Name")
                Glib::ustring nomSaisiGetustring = ligneFichierV.substr(7).c_str() ;
                if (nomSaisiGetustring  ==  nomSaisiGet)
                {
                    //si dans la ligne il trouve le nom saisie dpar utilisateur alors ce nom a ete été saisie

                   //la variable bool prend alors vrai
                   trouveDansFichierV = true ;
                   break ;
                }
            }
        }
    }
    return trouveDansFichierV ;
}






