### Auteur
Maroua CHAHDIL  

# But

Le but de ce projet a été de réaliser un programme en cpp permettant de créer une fiche de personnage basé sur un jeu rpg qui est **'Vampire The Masquerade'**.
Egalement, le fait de lire et de parcer le fichier contenant la notice du jeu. Et ainsi, pouvoir l'utiliser dans le programme.

## Protocoles    :

1. Téléchargement du projet via gitlab
2. DéCompression du fichier zip
3. Aller dans le dossier vampireprojetcpp-master 
4. Choisir le bon dossier Makefile en fonction du systeme utilisée Mac ou Windows
5. Sortir le fichier Makefile et le mettre dans le même chemin que les fichiers du projets (cpp,h,txt)
6. Commande à réaliser dans le terminal :
    `make`
Commande permettant d'activer le make file
7. Commande pour executer le programme (MAC UNIQUEMENT):
    `./VP`


## Installation  :

En amont **installer** gtkmm, pour se faire suivre les instructions décrit ici :[site officiel](https://gtkmm.org/fr/download.html) ou bien un [autre site possible](https://www.gtk.org/docs/installations/).

- C++ version: **C++11**
- Utilisation du Makefile : make

- Réalisation du projet **sous Mac OS** Catalina Version 10.15.7 (19H15)



## Fichier necessaire : 
- **VampireV5.txt** : Contient la notice du jeu et lecture de ce fichier dans l'application.

- **Fenetre.cpp**   : Contient la partie graphique et le stockage de la saisie.
    - **Fenetre.h** : Header de Fenetre, contient les prototypes.
    -


- **Vampire.cpp**   : Lecture du fichier VampireV5.cpp avec les set et get adéquates
    - **Vampire.h** : header de Vampire contient les prototypes.
    - 


- **Makefile**   : permet de compiler plusieurs fichier cpp simultanément.
    - Ce projet dispose d'un Makefile pour Mac OS et un autre pour Windows.
    

- Remarque:
    - Pour éviter un fichier avec un nombre trop important de ligne les fichiers ont été décomposés d'où l'extention "fichier**.pcpp**"
    - Il y a **6** fichiers de type .pcpp **Fenetre** et **2** **Vampire**
    - Il n'est pas necessaire de les rajouter dans la Makefile comme on est sur **cpp11** la gestion de ces fichiers est automatiquement gérés par le langage.


## Librairie nécessaire  :

Librairie obligatoire pour cpp pour la gestion de flux et de fichier.
```
#include <iostream>  //Entree sortie d'un fichier 
#include <sstream>  // Gestion des chaine de caractere (dont caractere spéciaux)
#include <fstream>  // Ecriture d'un flux dans un fichier  (Gestion fluc du fichier)

```

Les tableau utilisés dans le projet sont de type **vecteur** d'où l'ajout de cette librairie. Egalement, utilisation de variable **string** par exemple lors de la lecture du fichier **VampireV5.txt** .
```
#include <string>
#include <vector>
```

La librairie **gtkmm3** ne sera pas appeler totalement d'où l'ajout de partie **spécifique** de la librairie. Cela évite de charge toute la librairie alors que certaines partie ne sont pas utilisé.
**Ainsi la compilation sera plus rapide.**
```
#include <gtkmm/window.h>
#include <gtkmm/button.h>
#include <gtkmm/stock.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/image.h>
#include <gtkmm/entry.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/main.h>

```

## Lecture du fichier VampireV5.cpp :

Le fichier **VampireV5.txt** est parsé par la méthode _ChargerTableauxClanNatureetc_ présent dans le fichier **Vampire.cpp** pour charger les zones de texte et liste déroulantes de l’application. 
Lors du lancement de l’application, le fichier **VampireV5** est lu que **une seule fois**, donc du début jusqu’à la fin. Toutes les informations qui caractéristisent le joueur sont stockées dans des tableaux de type vecteur.


Ces élements ont été extrait du fichier **VampireV5.txt** et affiché dans l'application.

Tableau sous forme de **vecteur** et utilisation **d'une liste déroulante** de type Gtk::Combo Text.

    -   Le tableau listClan contient les noms des clans. 
    -   Le tableau listNature  contient les noms des Natures. 
    -   Attribut :
        - Physique
            -   Le tableau listStrength  contient les valeurs de l’attribut Strength.
            -   Le tableau listDexterity contient les valeurs de l’attribut Dexterity.
            -   Le tableau listStamia contient les valeurs de l’attribut Stamina.
        - Social 
            -   Le tableau listCharisma contient les valeurs de l’attribut Charisma.
            -   Le tableau listManupilation contient les valeurs de l’attribut Manipulation.
            -   Le tableau listApprearance contient les valeurs de l’attribut Appearance.
        - Mental 
            -   Le tableau listPerception contient les valeurs de l’attribut Perception.
            -   Le tableau listIntelligence contient les valeurs de l’attribut Intelligence.
            -   Le tableau listWits contient les valeurs de l’attribut Wits.
        - Disciplines
            -   Le tableau listDiscipline contient les différentes Disciplines.

        - Merits & Flaws
            - Les tableaux listMerits, listMeritsType contient respectivement  les informations sur  Mérites et les Types.
            - Les tableaux listFlaw contient la liste des Flaws. Les types de Flaws sont les mêmes celles des Mérites. 
            - Le tableau listCost contient les valeurs des Cost qui le même que les Bonus.

        - Rituals Paths
            -   Le tableau listRituals contient les noms des Rituals. 
            -   Le tableau listPaths contient les noms des Paths.

        - La partie Autre de Expanded Backgrounds
            -   Le tableau listPaths contient les noms des Backgrounds pour cette partie ci.


    -   Remarque :
            -   Ajout dans le fichier du titre " ###Background " pour pouvoir l'afficher dans l’app car il semblait manquer dans le fichier **VampireV5**.



##  Gestion graphique :

Le principe des **boites** dans le programmes :
- Chaque sections est contenu dans une **HBox ou bien VBox** d'où l'appelation de boite.
- Chaque section posséde une boite spécifique cela permet un affichage fluide des données.
- Chaque boite est caractérisée par un numéro allant de **1 à 21**.

    - Voici la répartition :
        - boite1        VBox donc boite verticale contient toute les autres boites c'est **la boites principale**. Elle contient également le systeme de barre de défilement de l'application (**scrolledwindow**).
        - Le reste seront des boites Horizontales
        - boite2        Contient les zones à saisir (name, nature, clan  ).
        - boite3        Contient les zones à saisir (Player, Demener, chronide).
        - boite4        Contient les zones à saisir (Generation, concept, Sire).

        - boite5        Les attributs sont stockés dans cette boite.
        - boite6        Les abilities sont stockés dans cette boite.
        - boite7        La partie Avantages avec Disciplines et Backgrounds sont présents ici.
        - boite8        Stockage des éléments entre la partie Avantages et Merits and Flaws donc Humanity/Path, Health, Willpower, Experience Ect.
        - boite9        Contient la section Merits and Flaws.
        - boite10       Les autres traits sont contenus ici.
        - boite12       Contient les zones à saisir pour Experience  Derangements.
        - boite13       La partie combat est ici.
        - boite14       Contient les zones à saisir pour Expanded Backgrounds.
        - boite15       La section Possessions est stockée dans cette boite.
        - boit16        Blood Bonds/Vinculi section.
        - boite17       Contient les zones à saisir pour Havens.
        - boite18       La partie History est contenue dans cette boite.
        - boite19       La partie Description est contenue dans cette boite.
        - boite20       La partie Visuals est présente ici.
        - boite21       Rituals and Paths sont stocké dans cette boite. 

    - La boite11 n'existe pas, c'est un choix personelle.                  
                

- Remarque
    - choix de faire des listes défilante donc Gtk::combo Text et non des boutons radio pour **afficher les informations de la notice du jeu** c'est à dire le fichier **VampireV5.txt** cela a été fait dans, par exemple, la section attribut.
   

##  Utilisation graphique :

Le **nom** du joueur est **obligatoire** par ailleurs, **l’application n’autorise pas la saisi d’un même nom de joueur plusieurs fois**.
Tout les nom de joueurs sont stockés dans le fichier **fichierV.txt** ainsi que toute leurs caractéristiques.


    - Contrôle effectuer  :
        - Sur la section Atribut sur la répartition des points.
        - Sur la section Abilities sur la répartition des points.
        Le nombre de points a attribué est indiqué dans l'application via un Label spécifique.

        - Contrôle sur la partie Avantage :
            - A Chaque Disciplines et  Backgrounds doit correspondre à une valeur.
            - Si on choisit une Disciplines ou un Backgrounds la zone de texte à cotée doit être remplie.
            - Les valeurs saisi dans ces mêmes zones de texte doivent être des valeurs numérique.


        - A la fin de la saisie, l'utilisateur appuie sur le bouton "Valider" pour enregistrer la fiche du joueur personalisée.
            - Si il y a des erreurs ou des conditions non respectées dans sa saisi alors une boite de dialogues s'affiche pour inviter l'utilisateur a corriger sa saisi. 




    - Choix de ne pas dupliquer certaines zone de texte pour éviter l'augmentation des lignes de code sachant que c'est le même principes à chaque fois. D'où le fait de le réaliser au moins une fois pour montrer le principe.

**IMPORTANT**
La commande *append* sur Mac devient *append_text* sur Windows. 
Cette commande permet l'ajout d'élément dans une liste déroulante de type *Gtk::Combo Text.
**Si on est sur Windows il faut alors remplacer tout les append par append_text.**

## **Fenetre.cpp**

- Class **Fenetre** 
    - Contient tout les parametres de la fenetre c'est à dire les boutons utilisé, label, entry, ComboText, ect.
    - Donc la déclaration des widgets se fait dans cette classe.
    
    - **Remarque** : 
        - Attribut 
            - Physique
                -   Le tableau tabStrength contient les valeurs de l’attribut Strength extrait du fichier **VampireV5.txt**.
                -   Le tableau tabDexterity contient les valeurs de l’attribut Dexterity extrait du fichier **VampireV5.txt**.
                -   Le tableau tabStamina contient les valeurs de l’attribut Stamina extrait du fichier **VampireV5.txt**.

        - Avantage
        -   Le tableau tabBackground contient les valeurs de l’attribut Background extrait du fichier **VampireV5.txt**.
        -   Le tableau tabDisciplines contient les valeurs de l’attribut Disciplines extrait du fichier **VampireV5.txt**.
        -   **ect.** **En effet c'est le même principe pour les autres sections** (Rituals Path, Merits&Flaws ... )


        - Donc ces tableaux permetent de d'afficher les informations extraites du fichier **VampireV5.txt** gràce à la classe **TableCategories** via la méthode _ChargerTableauxClanNatureetc_ de Vampire.cpp

- Class **VampireClass**
    - Permet de stocker la saisie effectuée par l'utilisateur.
    - Contient des Get et Set de chaque élement qui compose l'application.
        - Permet d'afficher et de stocker les informations saisie par l'utilisateur dans le fichier **fichierV.txt**

## **Vampire.cpp** : 

- Class **TableCategories** 
    - Composée de tableaux de type **vecteur** `<vector>`.
    - Permet de lire le fichier **VampireV5.txt** **en une fois ** et d'**extraire** les données interessants qui seront **afficher dans l'application**.
    - Systeme de **sauvegarde** mis en place grace a la methode : *ChargerTableauxClanNatureetc*
        - **Toute les infos données par l'utilisateur seront sauvegarder dans un fichier text** (**fichierV.txt**) dans le but de pouvoir les afficher en aval.

    - Getter et Setter pour chaque element extrait du fichier **VampireV5.txt** permet la gestion des informations extraites donc stockage et affichage dans l'application.




## Amélioration possible :

- Afficher plus de caracteristique sur les clan via la class **Clan** de  **Vampire.cpp**
    - **Code réaliser présent en commentaires**
    - Permet d'**afficher** dans l'application toute les données liée au clan qui sont stockées dans la class Clan.

- Realiser une interface permettant d'afficher la page du personnage avec les caractéristiques donnée par l'utilisateur qui ont été sauvegarder en amont dans le fichier **fichierV.txt**. Actuellement on a le fichier text **fichierV.txt** qui contient ces informations d'où une interface permettant de rendre le visuel plus esthétique.

- Gestion d'une **base d'information** de l'**ensemble des joueurs** **stockées** dans le fichier **fichierV.txt**. **Par exemple**, faire des **statistiques** sur les joueurs , analyser leurs **caractéristiques**: combien de personne appartient au clan n, ayant l'attribut physique de type force,la proportion de points moyennes données ect.










